from priodict import priorityDictionary
from collections import defaultdict 



class Graph(object):
    def __init__(self):
        self.nodes = {}
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.update(value)

    def add_edge(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.distances[(from_node, to_node)] = distance

    def remove_node(self, value):
        '''
        Remove node and edges related to node
        '''
        # remove edge
        del self.edges[value]
        # remove info about this node in other nodes dict
        for k, v in self.edges.iteritems():
            # remove relation between removal node and others
            v = [x for x in v if x != value]
            # update dict of node which has relation with removal node
            self.edges[k] = v 
        # remove distances
        remove_items = []
        for item in self.distances:
            if value in item:
                remove_items.append(item)
        for item in remove_items:
            del self.distances[item]
        # finally remove node
        self.nodes.remove(value)


    def _import(self, graph):
        ''' Importing graph tool '''
        for item in graph:
            self.add_node({item['node']:item['loc']})
            for i in item['edges']:
                self.add_edge(item['node'], i[0], i[1])
                self.add_edge(i[0], item['node'], i[1])




class PathFinder(object):

    def dijkstra(self, graph, initial):
     visited = {initial: 0}
     path = {}

     nodes = set(graph.nodes)

     while nodes: 
       min_node = None
       for node in nodes:
         if node in visited:
           if min_node is None:
             min_node = node
           elif visited[node] < visited[min_node]:
             min_node = node

       if min_node is None:
         break

       nodes.remove(min_node)
       current_weight = visited[min_node]

       for edge in graph.edges[min_node]:
         weight = current_weight + graph.distances[(min_node, edge)]
         if edge not in visited or weight < visited[edge]:
           visited[edge] = weight
           path[edge] = min_node

     return visited, path


    def shortestPath(self, graph, initial_node, goal_node):
        distances, paths = self.dijkstra(graph, initial_node)
        route = [goal_node]
     
        while goal_node != initial_node:
            route.append(paths[goal_node])
            goal_node = paths[goal_node]
     
        route.reverse()
        return route
     
     
