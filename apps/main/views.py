# -*- coding: utf-8 -*-
import json
from datetime import date

from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.shortcuts import render, render_to_response, get_object_or_404, get_list_or_404
from django.template.context import RequestContext
from django.utils.translation import ugettext as _
from django.conf import settings
from django.utils import translation
from django.contrib.gis.geos import GeometryCollection
from django.core.urlresolvers import reverse
from django.db.models import Q

from sorl.thumbnail import get_thumbnail
from haystack.views import SearchView

from main.models import Carousel, Shop, ShopGallery, Unit, Terminal, Category, Stair
from weblog.models import Post
from graph import Graph, PathFinder

def index(request):
    #items = Carousel.objects.filter(language=translation.get_language())
    #ctx = {'items': items}
    categories = []
    current_date = date.today()
    ctx = {'categories':categories, 'current_date':current_date}
    for cat_type in Category.CAT_TYPES:
        item = {}
        item = {'title':cat_type[1], 'slug':cat_type[0]}
        ctx['categories'].append(item)
    return render_to_response('index.html', ctx, context_instance=RequestContext(request))

def home(request):
    #items = Carousel.objects.filter(language=translation.get_language())
    #ctx = {'items': items}
    categories = []
    current_date = date.today()
    ctx = {'categories':categories, 'current_date':current_date}
    for cat_type in Category.CAT_TYPES:
        item = {}
        item = {'title':cat_type[1], 'slug':cat_type[0]}
        ctx['categories'].append(item)
    return render_to_response('home.html', ctx, context_instance=RequestContext(request))


def about(request):
    items = Carousel.objects.all().order_by('-id')
    ctx = {'items': items}
    return render_to_response('about.html', ctx, context_instance=RequestContext(request))

def test_about(request):
    items = Carousel.objects.all().order_by('-id')
    ctx = {'items': items}
    return render_to_response('test_about.html', ctx, context_instance=RequestContext(request))
    
def gurman(request):
    return render_to_response('gurman.html', context_instance=RequestContext(request))

def langredirect(request, lang_code):
    import re

    """
    Patched for Get method 
    """
    # get lang from url
    lang_code = re.split('/lang\/|\/', request.path)
    lang_code = lang_code[1]
    redirect = request.GET.get('next', '')
    response = HttpResponseRedirect(redirect)
    if lang_code:
        if hasattr(request, 'session'):
            request.session['django_language'] = lang_code
        else:
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
        return response

def gallery(request, slug):
    shop = get_object_or_404(Shop, slug=slug)
    images = ShopGallery.objects.filter(shop=shop)
    split_images = [images[x:x+8] for x in range(0, len(images), 8)]
    return render_to_response('gallery.html', locals(), context_instance=RequestContext(request))

def category(request, type, slug):
    cat = get_object_or_404(Category, type=type, slug=slug)
    shops = Shop.objects.filter(category=cat)
    split_shops = [shops[x:x+8] for x in range(0, len(shops), 8)]
    return render_to_response('category.html', locals(), context_instance=RequestContext(request))


def categories(request, type):
    categories = Category.objects.filter(type=type).order_by('order')
    for cat_type in Category.CAT_TYPES:
        if cat_type[0] == type:
            type = cat_type
    split_categories = [categories[x:x+8] for x in range(0, len(categories), 8)]
    return render_to_response('categories.html', locals(), context_instance=RequestContext(request))


def shop(request, slug):
    shop = get_object_or_404(Shop, slug=slug)
    categories = Category.objects.filter(shop=shop)
    news = Post.objects.filter(shop=shop).order_by('-created_at', '-updated_at')[:3]
    images = ShopGallery.objects.filter(shop=shop)
    return render_to_response('shop.html', locals(), context_instance=RequestContext(request))



def map(request, slug=None):
    ctx = {}
    if 'category' in request.GET and request.GET['category']:
        category = get_object_or_404(Category, slug=request.GET['category'])
        ctx.update({'cat_type':str(category.type), 'cat_slug':str(category.slug), 'category':category})
        shops = Shop.objects.filter(category=category)
    elif slug:
        shops = Shop.objects.filter(slug=slug)
    else:
        shops = Shop.objects.none()
    unit_ids = []
    for shop in shops:
        unit_ids.append(shop.unit.unit_id)
    units_level1 = Unit.objects.filter(unit_id__in = unit_ids, floor=1)
    geo_json_level1 = {"type": "FeatureCollection",
                    "features":[{
                        "type": "Feature",
                        "id": unit.unit_id,
                        "xlat": unit.xlat,
                        "ylat": unit.ylat,
                        "title_x": unit.title_x,
                        "title_y": unit.title_y,
                        "rotated_title":unit.rotated_title,
                        "font_size":shops.get(unit=unit).font_size,
                        "shop_url":reverse('shop', args=[shops.get(unit=unit).slug]),
                        "content": shops.get(unit=unit).map_title,
                        "name": shops.get(unit=unit).title,
                        "floor": unit.floor,
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [[list(coord) for coord in unit.poly.coords[0]]] }}
                        for unit in units_level1 
                    ]
                }
    units_level2 = Unit.objects.filter(unit_id__in = unit_ids, floor=2)
    geo_json_level2 = {"type": "FeatureCollection",
                    "features":[{
                        "type": "Feature",
                        "id": unit.unit_id,
                        "xlat": unit.xlat,
                        "ylat": unit.ylat,
                        "title_x": unit.title_x,
                        "title_y": unit.title_y,
                        "rotated_title":unit.rotated_title,
                        "font_size":shops.get(unit=unit).font_size,
                        "shop_url":reverse('shop', args=[shops.get(unit=unit).slug]),
                        "content": shops.get(unit=unit).map_title,
                        "floor": unit.floor,
                        "name": shops.get(unit=unit).title,
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [[list(coord) for coord in unit.poly.coords[0]]] }}
                        for unit in units_level2 
                    ]
                }
    ctx.update({'geo_json_level1':json.dumps(geo_json_level1), 'geo_json_level2':json.dumps(geo_json_level2)})
    return render_to_response('map.html', ctx, context_instance=RequestContext(request))

def test_map(request, slug=None):
    ctx = {}
    if 'category' in request.GET and request.GET['category']:
        category = get_object_or_404(Category, slug=request.GET['category'])
        ctx.update({'cat_type':str(category.type), 'cat_slug':str(category.slug), 'category':category})
        shops = Shop.objects.filter(category=category)
    elif slug:
        shops = Shop.objects.filter(slug=slug)
    else:
        # should be empty queryset. so this is only for testing
        # shops = Shop.objects.all()
        shops = Shop.objects.none()
    unit_ids = []
    for shop in shops:
        unit_ids.append(shop.unit.unit_id)
    units_level1 = Unit.objects.filter(unit_id__in = unit_ids, floor=1)
    geo_json_level1 = {"type": "FeatureCollection",
                    "features":[{
                        "type": "Feature",
                        "id": unit.unit_id,
                        "xlat": unit.xlat,
                        "ylat": unit.ylat,
                        "title_x": unit.title_x,
                        "title_y": unit.title_y,
                        "font_size":shops.get(unit=unit).font_size,
                        "rotated_title":unit.rotated_title,
                        "shop_url":reverse('shop', args=[shops.get(unit=unit).slug]),
                        "content": shops.get(unit=unit).map_title,
                        "name": shops.get(unit=unit).title,
                        "floor": unit.floor,
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [[list(coord) for coord in unit.poly.coords[0]]] }}
                        for unit in units_level1 
                    ]
                }
    units_level2 = Unit.objects.filter(unit_id__in = unit_ids, floor=2)
    geo_json_level2 = {"type": "FeatureCollection",
                    "features":[{
                        "type": "Feature",
                        "id": unit.unit_id,
                        "xlat": unit.xlat,
                        "ylat": unit.ylat,
                        "title_x": unit.title_x,
                        "title_y": unit.title_y,
                        "font_size":shops.get(unit=unit).font_size,
                        "rotated_title":unit.rotated_title,
                        "shop_url":reverse('shop', args=[shops.get(unit=unit).slug]),
                        "content": shops.get(unit=unit).map_title,
                        "name": shops.get(unit=unit).title,
                        "floor": unit.floor,
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [[list(coord) for coord in unit.poly.coords[0]]] }}
                        for unit in units_level2 
                    ]
                }
    ctx.update({'geo_json_level1':json.dumps(geo_json_level1), 'geo_json_level2':json.dumps(geo_json_level2)})
    return render_to_response('test_map.html', ctx, context_instance=RequestContext(request))

def get_map_data(request):
    '''
    Response to GET AJAX request with shops data for map
    '''
    if request.method == 'GET' and request.is_ajax():
        params = {}
        # get current terminal to show his location on map
        try:
            terminal = Terminal.objects.get(terminal_ip=request.META.get('REMOTE_ADDR'))
        except:
            # that's only for testing.
            # there should be some exception handler
            terminal = Terminal.objects.get(terminal_ip='127.0.0.1')
        stairs1 = Stair.objects.get(stair_id='stairs1')
        stairs2 = Stair.objects.get(stair_id='stairs2')
        params.update({
            'terminal_xlat': terminal.xlat,
            'terminal_ylat': terminal.ylat,
            'terminal_floor': terminal.floor,
            # 'stairs1_xlat': settings.STAIRS1_LOC[0],
            # 'stairs1_ylat': settings.STAIRS1_LOC[1],
            # 'stairs2_xlat': settings.STAIRS2_LOC[0],
            # 'stairs2_ylat': settings.STAIRS2_LOC[1],
            'stairs1_xlat': stairs1.xlat,
            'stairs1_ylat': stairs1.ylat,
            'stairs2_xlat': stairs2.xlat,
            'stairs2_ylat': stairs2.ylat,
            })
        # don't show shops title once again
        # it's already showed with geojson so no reason to display it
        if 'excluded_shop_data' in request.GET and request.GET['excluded_shop_data']:
            excluded_shop_data = request.GET['excluded_shop_data']
            if 'category' in excluded_shop_data:
                category = excluded_shop_data.split('?')[1]
                category_slug = category.split('=')[1]
                category = Category.objects.get(slug=category_slug)
                shop_set = Shop.objects.all().exclude(category=category)
            else:
                shop_slug = excluded_shop_data.split('/')[-2]
                shop_set = Shop.objects.all().exclude(slug = shop_slug)
        shops = []
        for shop in shop_set:
            unit = Unit.objects.get(id=shop.unit.id)
            item = {
                    'name': shop.title,
                    'content': shop.map_title,
                    "shop_url":reverse('shop', args=[shop.slug]),
                    'info': shop.content,
                    # 'xlat': unit.xlat,
                    # 'ylat': unit.ylat,
                    'unit_id': unit.unit_id,
                    "xlat": unit.xlat,
                    "ylat": unit.ylat,
                    "floor": unit.floor,
                    "title_x": unit.title_x,
                    "title_y": unit.title_y,
                    "font_size":shop.font_size,
                    "rotated_title": unit.rotated_title,
                    "features":[{
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [[list(coord) for coord in unit.poly.coords[0]]] }
                    }]
                }
            if shop.icon:
                icon = get_thumbnail(shop.icon, 'x40', upscale=False, quality=99)
                item.update({
                    'icon_url': icon.url,
                    'icon_size': [icon.width, icon.height],
                })
            shops.append(item)
        params.update({"shops": shops})
    else:
        return HttpResponseNotFound('<h1>Page not found</h1>')
    data = json.dumps(params)
    return HttpResponse(data, mimetype='application/json')


def navigate(request):
    if request.method == 'GET' and request.is_ajax():
        G = Graph()
        G._import(settings.CORNERS)
        params = {}
        # add stairs from db
        stairs = Stair.objects.all()
        for stair in stairs:
            G.add_node({stair.stair_id: [stair.xlat, stair.ylat]})
            # 1 floor nodes
            G.add_edge(stair.stair_id, stair.lnode1, stair.lnode1_edge)
            G.add_edge(stair.lnode1, stair.stair_id, stair.lnode1_edge)
            G.add_edge(stair.stair_id, stair.rnode1, stair.rnode1_edge)
            G.add_edge(stair.rnode1, stair.stair_id, stair.rnode1_edge)
            # 2 second floor nodes
            G.add_edge(stair.stair_id, stair.lnode2, stair.lnode2_edge)
            G.add_edge(stair.lnode2, stair.stair_id, stair.lnode2_edge)
            G.add_edge(stair.stair_id, stair.rnode2, stair.rnode2_edge)
            G.add_edge(stair.rnode2, stair.stair_id, stair.rnode2_edge)
        try:
            terminal = Terminal.objects.get(terminal_ip=request.META.get('REMOTE_ADDR'))
        except:
            # that's only for testing.
            # there should be some exception handler
            terminal = Terminal.objects.get(terminal_ip='127.0.0.1')
        if 'unit' in request.GET:
            unit = Unit.objects.get(unit_id=request.GET['unit'])
            if terminal.floor != unit.floor:
                # check if stairs and shop unit is on same hall (has same nearest nodes)
                # so we need to add edge between them not nearest corners
                if unit.floor == 1:
                    stair = Stair.objects.get( Q(lnode1=unit.lnode) | Q(rnode1=unit.rnode) )
                    if unit.lnode == stair.lnode1 and unit.rnode == stair.rnode1:
                        if unit.lnode == stair.lnode1:
                            # stair and shop has same lnode
                            G.add_node({'unit': [unit.xlat, unit.ylat, unit.floor]})
                            G.add_node({stair.stair_id: [stair.xlat, stair.ylat]})
                            G.add_edge(stair.stair_id, 'unit', abs(unit.lnode_edge - stair.lnode1_edge))
                            G.add_edge('unit', stair.stair_id, abs(unit.lnode_edge - stair.lnode1_edge))
                        elif unit.rnode == stair.rnode1:
                            # stair and shop has same rnode
                            G.add_node({'unit': [unit.xlat, unit.ylat, unit.floor]})
                            G.add_node({stair.stair_id: [stair.xlat, stair.ylat]})
                            G.add_edge(stair.stair_id, 'unit', abs(unit.rnode_edge - stair.rnode1_edge))
                            G.add_edge('unit', stair.stair_id, abs(unit.rnode_edge - stair.rnode1_edge))
                elif unit.floor == 2:
                    stair = Stair.objects.get( Q(lnode2=unit.lnode) | Q(rnode2=unit.rnode) )
                    if unit.lnode == stair.lnode2 and unit.rnode == stair.rnode2:
                        if unit.lnode == stair.lnode2:
                            # terminal and shop has same lnode
                            G.add_node({'unit': [unit.xlat, unit.ylat, unit.floor]})
                            G.add_node({stair.stair_id: [stair.xlat, stair.ylat]})
                            G.add_edge(stair.stair_id, 'unit', abs(unit.lnode_edge - stair.lnode2_edge))
                            G.add_edge('unit', stair.stair_id, abs(unit.lnode_edge - stair.lnode2_edge))
                        elif unit.rnode == stair.rnode2:
                            # terminal and shop has same rnode
                            G.add_node({'unit': [unit.xlat, unit.ylat, unit.floor]})
                            G.add_node({stair.stair_id: [stair.xlat, stair.ylat]})
                            G.add_edge(stair.stair_id, 'unit', abs(unit.rnode_edge - stair.rnode2_edge))
                            G.add_edge('unit', stair.stair_id, abs(unit.rnode_edge - stair.rnode2_edge))
            # check if terminal and shop unit is on same hall (has same nearest nodes)
            # so we need to add edge between them not nearest corners
            elif unit.lnode == terminal.lnode and unit.rnode == terminal.rnode:
                if unit.lnode == terminal.lnode:
                    # terminal and shop has same lnode
                    G.add_node({'unit': [unit.xlat, unit.ylat]})
                    G.add_node({'terminal': [terminal.xlat, terminal.ylat]})
                    G.add_edge('terminal', 'unit', unit.lnode_edge - terminal.lnode_edge)
                    G.add_edge('unit', 'terminal', unit.lnode_edge - terminal.lnode_edge)
                elif unit.rnode == terminal.rnode:
                    # terminal and shop has same rnode
                    G.add_node({'unit': [unit.xlat, unit.ylat]})
                    G.add_node({'terminal': [terminal.xlat, terminal.ylat]})
                    G.add_edge('terminal', 'unit', unit.rnode_edge - terminal.rnode_edge)
                    G.add_edge('unit', 'terminal', unit.rnode_edge - terminal.rnode_edge)
            # add terminal and unit nodes
            # add node based on unit data
            G.add_node({'unit': [unit.xlat, unit.ylat, unit.floor]})
            # add edge to nearest left node
            G.add_edge(unit.lnode, 'unit', unit.lnode_edge)
            G.add_edge('unit', unit.lnode, unit.lnode_edge)
            # add edge to nearest right node
            G.add_edge(unit.rnode, 'unit', unit.rnode_edge)
            G.add_edge('unit', unit.rnode, unit.rnode_edge)
            # add node based on terminal data
            G.add_node({'terminal': [terminal.xlat, terminal.ylat, terminal.floor]})
            # add nearest left node to terminal
            G.add_edge(terminal.lnode, 'terminal', terminal.lnode_edge)
            G.add_edge('terminal', terminal.lnode, terminal.lnode_edge)
            # add nearest right node to terminal
            G.add_edge(terminal.rnode, 'terminal', terminal.rnode_edge)
            G.add_edge('terminal', terminal.rnode, terminal.rnode_edge)
            nav_to = 'unit'
        else:
            # for testing only
            nav_to = request.GET['to']
            # TODO: show error message
        p = PathFinder()
        route = p.shortestPath(G, 'terminal', nav_to)
        coord = {}
        for k, v in G.nodes.iteritems():
            if k in route:
                coord.update({k: v})
        params.update({'path': route, 'coord': coord})
    else:
        return HttpResponseNotFound('<h1>Page not found</h1>')
    data = json.dumps(params)
    return HttpResponse(data, mimetype='application/json')


class CustomSearchView(SearchView):
    def create_response(self):
        """
        Generates the actual HttpResponse to send back to the user.
        """
        #super(SearchView, self).create_response()
        #(paginator, page) = self.build_page()
        split_results = [self.results[x:x+4] for x in range(0, len(self.results), 4)]
        #print '================ split_results', split_results
        context = {
            'query': self.query,
            'form': self.form,
            # 'page': page,
            # 'paginator': paginator,
            'suggestion': None,
            'split_results':split_results
        }


        if self.results and hasattr(self.results, 'query') and self.results.query.backend.include_spelling:
            context['suggestion'] = self.form.get_suggestion()

        context.update(self.extra_context())
        return render_to_response(self.template, context, context_instance=self.context_class(self.request))