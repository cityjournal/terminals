# -*- coding: utf-8 -*-
import subprocess
import re
from decimal import Decimal

#from django.db import models
from django.contrib.gis.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.validators import MaxLengthValidator, MinValueValidator

from sorl.thumbnail import ImageField, get_thumbnail
from tinymce.models import HTMLField



class Playlist(models.Model):
    title = models.CharField(max_length=125)
    slug = models.SlugField(unique=True)
    #duration = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))], default=0.0, blank=True)
    date = models.DateField()

    def __unicode__(self):
        return self.title

    @property
    def get_duration(self):
        hours = 0
        minutes = 0
        seconds = 0
        videos = Video.objects.filter(playlist=self.id)
        for video in videos:
            duration = video.get_video_length()
            hours += Decimal(duration['hours'])
            minutes += Decimal(duration['minutes'])
            seconds += Decimal(duration['seconds'])
        
        if seconds > 60:
            #sec_to_minutes = seconds/60
            sec_to_minutes, seconds= divmod(seconds, 60)
            minutes += sec_to_minutes
            #seconds = seconds%60
        
        if minutes > 60:
            #min_to_hours = minutes/60
            min_to_hours, minutes= divmod(seconds, 60)
            hours += min_to_hours
            #minutes = minutes%60
        return {'hours':'{0:.3g}'.format(hours), 'minutes':'{0:.3g}'.format(minutes), 'seconds':'{0:.3g}'.format(seconds)}

class Video(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    playlist = models.ForeignKey(Playlist)
    file = models.FileField(upload_to="uploads/playlists/")
    slug = models.SlugField(blank=True, null=True)
    order = models.PositiveIntegerField(blank=True, null=True)
    
    class Meta:
        verbose_name = _("Video file")
        verbose_name_plural = _("Videos from playlist")

    # def save(self, *args, **kwargs):
    #     # to get duration ffmpeg should be installed with sudo apt-get install ffmpeg
    #     super(Video, self).save(*args, **kwargs)
    #     if self.file and self.playlist:
    #         #result = subprocess.Popen(["ffprobe", str(self.file.path)], stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    #         #matches = re.search(r"Duration:\s{1}(?\d+):(?\d+):(?\d+\.\d+)", result, re.DOTALL).groupdict()

    #         process = subprocess.Popen(['ffmpeg', '-i', str(self.file.path)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #         stdout, stderr = process.communicate()
    #         matches = re.search(r"Duration:\s{1}(?P<hours>\d+?):(?P<minutes>\d+?):(?P<seconds>\d+\.\d+?),", stdout, re.DOTALL).groupdict()
 
    #         hours = Decimal(matches['hours'])
    #         minutes = Decimal(matches['minutes'])
    #         seconds = Decimal(matches['seconds'])
         
    #         total = 0
    #         total += 60 * 60 * hours
    #         total += 60 * minutes
    #         total += seconds
    #         playlist = Playlist.objects.get(id=self.playlist.id)
    #         playlist.duration += total
    #         playlist.save()
    
    def delete(self, *args, **kwargs):
         # You have to prepare what you need before delete the model
         storage, path = self.file.storage, self.file.path
         # Delete the model before the file
         super(Video, self).delete(*args, **kwargs)
         # Delete the file after the model
         storage.delete(path)

    def __unicode__(self):
        if self.title:
            return self.title
        else:
            return str(self.id)

    def as_dict(self):
        return {
            'id':self.id,
            'order':self.order
        }

    def save(self, *args, **kwargs):
        if not self.pk:
            self.order = len(Video.objects.all())
        super(Video, self).save(*args, **kwargs)


    def get_video_length(self):
        if self.file:
            process = subprocess.Popen(['ffmpeg', '-i', str(self.file.path)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            stdout, stderr = process.communicate()
            matches = re.search(r"Duration:\s{1}(?P<hours>\d+?):(?P<minutes>\d+?):(?P<seconds>\d+\.\d+?),", stdout, re.DOTALL).groupdict()
            return matches



class Carousel(models.Model):
    title = models.CharField(max_length=25)
    link = models.CharField(max_length=255, blank=True)
    content = HTMLField(blank=True)
    image = ImageField(upload_to="uploads/carousel/", blank=True)
    language = models.CharField(max_length=15, choices=settings.LANGUAGES, default="ru")
    
    class Meta:
        verbose_name = _("Carousel content")
        verbose_name_plural = verbose_name
    
    def __unicode__(self):
        return self.title

class Terminal(models.Model):
    terminal_ip = models.GenericIPAddressField()
    xlat = models.FloatField()
    ylat = models.FloatField()
    lnode = models.CharField(max_length=10, help_text="nearest left corner")
    lnode_edge = models.FloatField(help_text="distance to left corner")
    rnode = models.CharField(max_length=10, help_text="nearest right corner")
    rnode_edge = models.FloatField(help_text="distance to right corner")
    floor = models.IntegerField(default=1)
    def __unicode__(self):
        return self.terminal_ip

class Stair(models.Model):
    stair_id = models.CharField(max_length=50, unique=True)
    xlat = models.FloatField()
    ylat = models.FloatField()
    lnode1 = models.CharField(max_length=10, help_text="nearest left corner on 1 floor")
    lnode1_edge = models.FloatField(help_text="distance to left corner on 1 floor")
    rnode1 = models.CharField(max_length=10, help_text="nearest right corner on 1 floor")
    rnode1_edge = models.FloatField(help_text="distance to right corner on 1 floor")
    lnode2 = models.CharField(max_length=10, help_text="nearest left corner on 2 floor")
    lnode2_edge = models.FloatField(help_text="distance to left corner on 2 floor")
    rnode2 = models.CharField(max_length=10, help_text="nearest right corner on 2 floor")
    rnode2_edge = models.FloatField(help_text="distance to right corner on 2 floor")

    def __unicode__(self):
        return self.stair_id

class Unit(models.Model):
    unit_id = models.IntegerField(unique=True)
    xlat = models.FloatField(help_text="doors coords")
    ylat = models.FloatField(help_text="doors coords")
    title_x = models.FloatField(help_text="title coords")
    title_y = models.FloatField(help_text="title coords")
    rotated_title = models.BooleanField(default=False, help_text="display vertical or horizontal title")
    lnode = models.CharField(max_length=10, help_text="nearest left corner")
    lnode_edge = models.FloatField(help_text="distance to left corner")
    rnode = models.CharField(max_length=10, help_text="nearest right corner")
    rnode_edge = models.FloatField(help_text="distance to right corner")
    floor = models.IntegerField(default=2)
    poly = models.PolygonField(spatial_index=False)
    objects = models.GeoManager()
    def __unicode__(self):
        return '%.5d' % self.unit_id

class Category(models.Model):
    CAT_TYPES = (
        ('market', (u'Продуктовый супермаркет')),
        ('shops', (u'Магазины')),
        ('products-for-home', (u'Товары для дома')),
        ('services', (u'Услуги')),
        ('restaurants', (u'Рестораны и кафе')),
        ('fun', (u'Развлечения')),
        ('other', (u'Прочее')),
        )
    title = models.CharField(_(u'Название'), max_length=100)
    slug = models.SlugField(_(u'URL'), max_length=100, unique=True)
    image = ImageField(_(u'Иконка в меню'), upload_to="uploads/catalogue/сategory/images/")
    main_image = ImageField(_(u'Иконка в каталоге'), upload_to="uploads/catalogue/category/images/")
    #language = models.CharField(_(u'Язык контента'), max_length=15, choices=settings.LANGUAGES, default="ru")
    type = models.CharField(_(u'Категория'), max_length=100, choices=CAT_TYPES, default=CAT_TYPES[1][0])
    order = models.IntegerField(_(u'Последовательность'), default=0)
    
    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __unicode__(self):
        return self.title


class Shop(models.Model):
    title = models.CharField(_(u'Название'), max_length=255)
    slug = models.SlugField(_(u'URL'), unique=True)
    image = ImageField(_(u'Картинка'), upload_to="uploads/catalogue/shops/images/", blank=True, null=True)
    content = models.TextField(validators=[MaxLengthValidator(715)], blank=True)
    #content = models.TextField(blank=True)
    map_title = models.TextField(_(u'Название на карте'), blank=True)
    font_size = models.PositiveIntegerField(default=9)
    unit = models.ForeignKey(Unit, verbose_name=_(u'Юнит'))
    icon = ImageField(_(u'Иконка на карте'), upload_to="uploads/catalogue/shops/icons/", help_text=_("Icon for map"), blank=True, null=True)
    category = models.ManyToManyField(Category, verbose_name=_(u'Категория'))
    open_time = models.TimeField(_(u'Время открытия'), blank=True, null=True)
    close_time = models.TimeField(_(u'Время закрытия'), blank=True, null=True)
    discount = models.BooleanField(_(u'Скидки'), default=False)
    #video = models.FileField(_(u'Видео'), upload_to="uploads/catalogue/shops/videos/", blank=True, null=True)
    #language = models.CharField(_(u'Язык контента'), max_length=15, choices=settings.LANGUAGES, default="ru")
    background = ImageField(_(u'Фон'), upload_to="uploads/catalogue/shops/images/", blank=True, null=True)
    #floor = models.IntegerField(default=1)

    class Meta:
        verbose_name = _("Shop")
        verbose_name_plural = _("Shops")
    
    def __unicode__(self):
        return self.title

    
    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return "<img src='%s'>" % im.url
        return ''
    preview.allow_tags = True
    
    def get_absolute_url(self):
        return reverse('shop', args=[self.slug])


class ShopGallery(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True)
    shop = models.ForeignKey(Shop, blank=True)
    file = ImageField(upload_to="uploads/catalogue/shops/")
    #slug = models.SlugField(blank=True)
    class Meta:
        verbose_name = _("Shop image gallery")
        verbose_name_plural = _("Shop images gallery")
    
    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('upload-new', )

    def save(self, *args, **kwargs):
        if not self.title:
            self.title = self.file.name
        #self.slug = self.file.name
        super(ShopGallery, self).save(*args, **kwargs)

    # def delete(self, *args, **kwargs):
    #     """delete -- Remove to leave file."""
    #     self.file.delete(False)
    #     super(ShopGallery, self).delete(*args, **kwargs)

    def preview(self):
        if self.file:
            im = get_thumbnail(self.file, '100x100', crop='center')
            return im.url
        return ''
    preview.allow_tags = True