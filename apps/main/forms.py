# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.utils.translation import ugettext, ugettext_lazy as _
from modeltranslation.forms import TranslationModelForm
from modeltranslation.widgets import ClearableWidgetWrapper
from tinymce.widgets import TinyMCE
#from django.contrib.auth.forms import AuthenticationForm as BaseAuthForm

from main.models import Shop, Category, ShopGallery, Video, Playlist

class PlaylistForm(forms.ModelForm):
    date = forms.DateField(input_formats=('%d-%m-%Y',), widget=forms.DateInput(format='%d-%m-%Y'))
    #duration = forms.CharField(widget=forms.HiddenInput())
    def __init__(self, *args, **kwargs):
        super(PlaylistForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.widget.attrs.update({'class':'form-control'})
    class Meta:
        model = Playlist

class VideoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VideoForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.widget.attrs.update({'class':'form-control'})
    class Meta:
        model = Video

class ShopGalleryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ShopGalleryForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.widget.attrs.update({'class':'form-control'})
    class Meta:
        model = ShopGallery

class CategoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})
    class Meta:
        model = Category


class ShopForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ShopForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            # if isinstance(field.widget, forms.TextInput) or \
            #     isinstance(field.widget, forms.Textarea) or \
            #     isinstance(field.widget, forms.DateInput) or \
            #     isinstance(field.widget, forms.DateTimeInput) or \
            #     isinstance(field.widget, forms.TimeInput):
            field.widget.attrs.update({'class': 'form-control'})

    #content = forms.CharField(label=u'Контент', widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
    #category = forms.ModelMultipleChoiceField(queryset=Category.objects, widget=forms.CheckboxSelectMultiple(), required=True)
    class Meta:
        model = Shop