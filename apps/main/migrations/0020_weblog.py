# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Carousel.content_en'
        db.delete_column(u'main_carousel', 'content_en')

        # Deleting field 'Carousel.title_en'
        db.delete_column(u'main_carousel', 'title_en')

        # Deleting field 'Carousel.content_uk'
        db.delete_column(u'main_carousel', 'content_uk')

        # Deleting field 'Carousel.content_ru'
        db.delete_column(u'main_carousel', 'content_ru')

        # Deleting field 'Carousel.title_ru'
        db.delete_column(u'main_carousel', 'title_ru')

        # Deleting field 'Carousel.title_uk'
        db.delete_column(u'main_carousel', 'title_uk')

        # Deleting field 'Category.title_en'
        db.delete_column(u'main_category', 'title_en')

        # Deleting field 'Category.title_ru'
        db.delete_column(u'main_category', 'title_ru')

        # Deleting field 'Category.title_uk'
        db.delete_column(u'main_category', 'title_uk')

        # Deleting field 'Event.content_en'
        db.delete_column(u'main_event', 'content_en')

        # Deleting field 'Event.title_en'
        db.delete_column(u'main_event', 'title_en')

        # Deleting field 'Event.content_uk'
        db.delete_column(u'main_event', 'content_uk')

        # Deleting field 'Event.content_ru'
        db.delete_column(u'main_event', 'content_ru')

        # Deleting field 'Event.title_ru'
        db.delete_column(u'main_event', 'title_ru')

        # Deleting field 'Event.title_uk'
        db.delete_column(u'main_event', 'title_uk')

        # Deleting field 'ShopGallery.title_en'
        db.delete_column(u'main_shopgallery', 'title_en')

        # Deleting field 'ShopGallery.description_en'
        db.delete_column(u'main_shopgallery', 'description_en')

        # Deleting field 'ShopGallery.description_uk'
        db.delete_column(u'main_shopgallery', 'description_uk')

        # Deleting field 'ShopGallery.title_ru'
        db.delete_column(u'main_shopgallery', 'title_ru')

        # Deleting field 'ShopGallery.title_uk'
        db.delete_column(u'main_shopgallery', 'title_uk')

        # Deleting field 'ShopGallery.description_ru'
        db.delete_column(u'main_shopgallery', 'description_ru')

        # Deleting field 'EventGallery.title_en'
        db.delete_column(u'main_eventgallery', 'title_en')

        # Deleting field 'EventGallery.description_en'
        db.delete_column(u'main_eventgallery', 'description_en')

        # Deleting field 'EventGallery.description_uk'
        db.delete_column(u'main_eventgallery', 'description_uk')

        # Deleting field 'EventGallery.title_ru'
        db.delete_column(u'main_eventgallery', 'title_ru')

        # Deleting field 'EventGallery.title_uk'
        db.delete_column(u'main_eventgallery', 'title_uk')

        # Deleting field 'EventGallery.description_ru'
        db.delete_column(u'main_eventgallery', 'description_ru')

        # Deleting field 'Shop.content_uk'
        db.delete_column(u'main_shop', 'content_uk')

        # Deleting field 'Shop.content_en'
        db.delete_column(u'main_shop', 'content_en')

        # Deleting field 'Shop.content_ru'
        db.delete_column(u'main_shop', 'content_ru')

        # Deleting field 'Shop.title_en'
        db.delete_column(u'main_shop', 'title_en')

        # Deleting field 'Shop.title_ru'
        db.delete_column(u'main_shop', 'title_ru')

        # Deleting field 'Shop.title_uk'
        db.delete_column(u'main_shop', 'title_uk')

        # Adding field 'Shop.language'
        db.add_column(u'main_shop', 'language',
                      self.gf('django.db.models.fields.CharField')(default='en', max_length=15),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Carousel.content_en'
        db.add_column(u'main_carousel', 'content_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Carousel.title_en'
        db.add_column(u'main_carousel', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Carousel.content_uk'
        db.add_column(u'main_carousel', 'content_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Carousel.content_ru'
        db.add_column(u'main_carousel', 'content_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Carousel.title_ru'
        db.add_column(u'main_carousel', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Carousel.title_uk'
        db.add_column(u'main_carousel', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Category.title_en'
        db.add_column(u'main_category', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Category.title_ru'
        db.add_column(u'main_category', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Category.title_uk'
        db.add_column(u'main_category', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.content_en'
        db.add_column(u'main_event', 'content_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_en'
        db.add_column(u'main_event', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.content_uk'
        db.add_column(u'main_event', 'content_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.content_ru'
        db.add_column(u'main_event', 'content_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_ru'
        db.add_column(u'main_event', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_uk'
        db.add_column(u'main_event', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.title_en'
        db.add_column(u'main_shopgallery', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.description_en'
        db.add_column(u'main_shopgallery', 'description_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.description_uk'
        db.add_column(u'main_shopgallery', 'description_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.title_ru'
        db.add_column(u'main_shopgallery', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.title_uk'
        db.add_column(u'main_shopgallery', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.description_ru'
        db.add_column(u'main_shopgallery', 'description_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'EventGallery.title_en'
        db.add_column(u'main_eventgallery', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'EventGallery.description_en'
        db.add_column(u'main_eventgallery', 'description_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'EventGallery.description_uk'
        db.add_column(u'main_eventgallery', 'description_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'EventGallery.title_ru'
        db.add_column(u'main_eventgallery', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'EventGallery.title_uk'
        db.add_column(u'main_eventgallery', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'EventGallery.description_ru'
        db.add_column(u'main_eventgallery', 'description_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.content_uk'
        db.add_column(u'main_shop', 'content_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.content_en'
        db.add_column(u'main_shop', 'content_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.content_ru'
        db.add_column(u'main_shop', 'content_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.title_en'
        db.add_column(u'main_shop', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.title_ru'
        db.add_column(u'main_shop', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.title_uk'
        db.add_column(u'main_shop', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Shop.language'
        db.delete_column(u'main_shop', 'language')


    models = {
        u'main.carousel': {
            'Meta': {'object_name': 'Carousel'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'main.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.event': {
            'Meta': {'object_name': 'Event'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'main.eventgallery': {
            'Meta': {'object_name': 'EventGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'main.shop': {
            'Meta': {'object_name': 'Shop'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['main.Category']", 'null': 'True', 'blank': 'True'}),
            'close_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'discount': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'en'", 'max_length': '15'}),
            'open_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Unit']", 'null': 'True', 'blank': 'True'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'main.shopgallery': {
            'Meta': {'object_name': 'ShopGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'file': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Shop']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.terminal': {
            'Meta': {'object_name': 'Terminal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'terminal_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        },
        u'main.unit': {
            'Meta': {'object_name': 'Unit'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'unit_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['main']