# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Unit.title_x'
        db.add_column(u'main_unit', 'title_x',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)

        # Adding field 'Unit.title_y'
        db.add_column(u'main_unit', 'title_y',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Unit.title_x'
        db.delete_column(u'main_unit', 'title_x')

        # Deleting field 'Unit.title_y'
        db.delete_column(u'main_unit', 'title_y')


    models = {
        u'main.carousel': {
            'Meta': {'object_name': 'Carousel'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'ru'", 'max_length': '15'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'main.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'main_image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'shops'", 'max_length': '100'})
        },
        u'main.playlist': {
            'Meta': {'object_name': 'Playlist'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        u'main.shop': {
            'Meta': {'object_name': 'Shop'},
            'background': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['main.Category']", 'null': 'True', 'blank': 'True'}),
            'close_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'discount': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'icon': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'map_title': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'open_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Unit']"})
        },
        u'main.shopgallery': {
            'Meta': {'object_name': 'ShopGallery'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'file': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Shop']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.terminal': {
            'Meta': {'object_name': 'Terminal'},
            'floor': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'terminal_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        },
        u'main.unit': {
            'Meta': {'object_name': 'Unit'},
            'floor': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'poly': ('django.contrib.gis.db.models.fields.PolygonField', [], {'spatial_index': 'False'}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'title_x': ('django.db.models.fields.FloatField', [], {}),
            'title_y': ('django.db.models.fields.FloatField', [], {}),
            'unit_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        },
        u'main.video': {
            'Meta': {'object_name': 'Video'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'playlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Playlist']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['main']