# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Category.language'
        db.add_column(u'main_category', 'language',
                      self.gf('django.db.models.fields.CharField')(default='en', max_length=15),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Category.language'
        db.delete_column(u'main_category', 'language')


    models = {
        u'main.carousel': {
            'Meta': {'object_name': 'Carousel'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'main.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'en'", 'max_length': '15'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.event': {
            'Meta': {'object_name': 'Event'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'main.eventgallery': {
            'Meta': {'object_name': 'EventGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'main.shop': {
            'Meta': {'object_name': 'Shop'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['main.Category']", 'null': 'True', 'blank': 'True'}),
            'close_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'discount': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'en'", 'max_length': '15'}),
            'open_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Unit']", 'null': 'True', 'blank': 'True'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'main.shopgallery': {
            'Meta': {'object_name': 'ShopGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'file': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'en'", 'max_length': '15'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Shop']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.terminal': {
            'Meta': {'object_name': 'Terminal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'terminal_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        },
        u'main.unit': {
            'Meta': {'object_name': 'Unit'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'unit_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['main']