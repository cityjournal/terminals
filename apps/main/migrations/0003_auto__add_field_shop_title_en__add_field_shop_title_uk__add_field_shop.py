# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shop.title_en'
        db.add_column(u'main_shop', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.title_uk'
        db.add_column(u'main_shop', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.title_ru'
        db.add_column(u'main_shop', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.content_en'
        db.add_column(u'main_shop', 'content_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.content_uk'
        db.add_column(u'main_shop', 'content_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.content_ru'
        db.add_column(u'main_shop', 'content_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.title_en'
        db.add_column(u'main_shopgallery', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.title_uk'
        db.add_column(u'main_shopgallery', 'title_uk',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.title_ru'
        db.add_column(u'main_shopgallery', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.description_en'
        db.add_column(u'main_shopgallery', 'description_en',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.description_uk'
        db.add_column(u'main_shopgallery', 'description_uk',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'ShopGallery.description_ru'
        db.add_column(u'main_shopgallery', 'description_ru',
                      self.gf('tinymce.models.HTMLField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Shop.title_en'
        db.delete_column(u'main_shop', 'title_en')

        # Deleting field 'Shop.title_uk'
        db.delete_column(u'main_shop', 'title_uk')

        # Deleting field 'Shop.title_ru'
        db.delete_column(u'main_shop', 'title_ru')

        # Deleting field 'Shop.content_en'
        db.delete_column(u'main_shop', 'content_en')

        # Deleting field 'Shop.content_uk'
        db.delete_column(u'main_shop', 'content_uk')

        # Deleting field 'Shop.content_ru'
        db.delete_column(u'main_shop', 'content_ru')

        # Deleting field 'ShopGallery.title_en'
        db.delete_column(u'main_shopgallery', 'title_en')

        # Deleting field 'ShopGallery.title_uk'
        db.delete_column(u'main_shopgallery', 'title_uk')

        # Deleting field 'ShopGallery.title_ru'
        db.delete_column(u'main_shopgallery', 'title_ru')

        # Deleting field 'ShopGallery.description_en'
        db.delete_column(u'main_shopgallery', 'description_en')

        # Deleting field 'ShopGallery.description_uk'
        db.delete_column(u'main_shopgallery', 'description_uk')

        # Deleting field 'ShopGallery.description_ru'
        db.delete_column(u'main_shopgallery', 'description_ru')


    models = {
        u'main.carousel': {
            'Meta': {'object_name': 'Carousel'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'content_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'})
        },
        u'main.shop': {
            'Meta': {'object_name': 'Shop'},
            'content': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'main.shopgallery': {
            'Meta': {'object_name': 'ShopGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Shop']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['main']