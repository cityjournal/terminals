# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Terminal'
        db.create_table(u'main_terminal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('terminal_ip', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39)),
            ('xlat', self.gf('django.db.models.fields.FloatField')()),
            ('ylat', self.gf('django.db.models.fields.FloatField')()),
            ('lnode', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('lnode_edge', self.gf('django.db.models.fields.FloatField')()),
            ('rnode', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('rnode_edge', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'main', ['Terminal'])


    def backwards(self, orm):
        # Deleting model 'Terminal'
        db.delete_table(u'main_terminal')


    models = {
        u'main.carousel': {
            'Meta': {'object_name': 'Carousel'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'content_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'})
        },
        u'main.event': {
            'Meta': {'object_name': 'Event'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'content_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'main.eventgallery': {
            'Meta': {'object_name': 'EventGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.shop': {
            'Meta': {'object_name': 'Shop'},
            'content': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'content_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'content_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Unit']", 'null': 'True', 'blank': 'True'})
        },
        u'main.shopgallery': {
            'Meta': {'object_name': 'ShopGallery'},
            'description': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_uk': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Shop']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_uk': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.terminal': {
            'Meta': {'object_name': 'Terminal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'terminal_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        },
        u'main.unit': {
            'Meta': {'object_name': 'Unit'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'lnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'rnode': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'rnode_edge': ('django.db.models.fields.FloatField', [], {}),
            'unit_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'xlat': ('django.db.models.fields.FloatField', [], {}),
            'ylat': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['main']