class BellmanFord(object):

    def __init__(self, graph, source):
        self.graph = graph
        self.source = source

    # Step 1: For each node prepare the destination and predecessor
    def initialize(graph, source):
        d = {} # Stands for destination
        p = {} # Stands for predecessor
        for node in graph:
            d[node] = float('Inf') # We start admiting that the rest of nodes are very very far
            p[node] = None
        d[source] = 0 # For the source we know how to reach
        return d, p
     
    def relax(node, neighbour, graph, d, p):
        # If the distance between the node and the neighbour is lower than the one I have now
        if d[neighbour] > d[node] + graph[node][neighbour]:
            # Record this lower distance
            d[neighbour]  = d[node] + graph[node][neighbour]
            p[neighbour] = node
     
    def bellman_ford(graph, source):
        d, p = self.initialize(self.graph, self.source)
        for i in range(len(graph)-1): #Run this until is converges
            for u in self.graph:
                for v in self.graph[u]: #For each neighbour of u
                    self.relax(u, v, self.graph, d, p) #Lets relax it
     
        # Step 3: check for negative-weight cycles
        for u in self.graph:
            for v in self.graph[u]:
                assert d[v] <= d[u] + self.graph[u][v]
     
        return d, p
 
 
