#from django.contrib import admin
from django.contrib.gis import admin

from sorl.thumbnail.admin import AdminImageMixin
from mce_filebrowser.admin import MCEFilebrowserAdmin

from main.models import Carousel, Shop, ShopGallery, Unit, Terminal, Category, Playlist, Video, Stair

class VideoInline(admin.TabularInline):
    fieldsets = (
        (
            None, 
            {
                'fields': ('title', 'file', 'slug')
            }
        ),
    )

    model = Video
    extra = 0

class VideoAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('id', 'order', 'file')

class PlaylistAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    inlines = (VideoInline, )

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

class StairAdmin(admin.ModelAdmin):
    pass

class TerminalAdmin(admin.ModelAdmin):
    pass

class UnitAdmin(admin.ModelAdmin):
    list_display = ('unit_id', 'floor')
    list_filter = ('floor',)

class ShopImagesInline(AdminImageMixin, admin.TabularInline):
    """
    Gallery Image inline
    """
    fieldsets = (
        (
            None, 
            {
                'fields': ('title', 'file', )
            }
        ),
    )

    model = ShopGallery
    extra = 0



class CarouselAdmin(AdminImageMixin, MCEFilebrowserAdmin, admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title','link', 'content', 'image']}),
    ]


class ShopAdmin(AdminImageMixin, MCEFilebrowserAdmin, admin.ModelAdmin):
    inlines = (ShopImagesInline, )
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('category',)
    search_fields = ('title', )
    list_display = ('title', 'slug', 'preview', 'unit')

class ShopGalleryAdmin(AdminImageMixin, MCEFilebrowserAdmin, admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title','description', 'shop', 'file', ]}),
    ]



admin.site.register(Category, CategoryAdmin)
admin.site.register(Terminal, TerminalAdmin)
admin.site.register(Unit,  UnitAdmin)
admin.site.register(Carousel, CarouselAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(ShopGallery, ShopGalleryAdmin)
admin.site.register(Playlist, PlaylistAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Stair, StairAdmin)