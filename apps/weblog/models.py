# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _
from django.template.defaultfilters import truncatechars
from django.contrib.auth.models import User

from tinymce.models import HTMLField
from sorl.thumbnail import ImageField

from main.models import Shop

class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name





class Post(models.Model):
    POST_TYPES = (
        ('news', (u'Новости')),
        ('sales', (u'Акции'))
        )
    
    title = models.CharField(_(u'Название'), max_length=500)
    slug = models.SlugField(_(u'URL'), unique=True)
    intro = models.TextField()
    content = models.TextField()
    preview_image = ImageField(_(u'Картинка превью'), upload_to="uploads/post/", blank=True)
    image = ImageField(_(u'Главная картинка'), upload_to="uploads/post/", blank=True)
    tag = models.ManyToManyField(Tag, verbose_name=_(u'Теги'), blank=True, null=True)
    shop = models.ForeignKey(Shop, verbose_name=_(u'Магазин'))
    author = models.ForeignKey(User, verbose_name=_(u'Автор'), blank=True, null=True)
    created_at = models.DateTimeField(_(u'Создано'), auto_now_add=True)
    updated_at = models.DateTimeField(_(u'Изменино'), auto_now=True)
    #language = models.CharField(_(u'Язык контента'), max_length=15, choices=settings.LANGUAGES, default="ru")
    type = models.CharField(_(u'Тип'), max_length=15, choices=POST_TYPES, default=POST_TYPES[0][0])

    def __unicode__(self):
        return self.title



class Comment(models.Model):
    #author = models.ForeignKey(User)
    name = models.CharField(max_length=125)
    email = models.EmailField(blank=True, null=True)
    content = models.TextField()
    post = models.ForeignKey(Post)

    def __unicode__(self):
        return truncatechars(self.content, 60)