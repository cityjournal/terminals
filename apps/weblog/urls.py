from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
   
   url(r'^$', 'weblog.views.index', name='index'),
   url(r'^shop/(?P<shop>[a-zA-Z0-9_.-]+)/$', 'weblog.views.shop_posts', name='shop_posts'),
   url(r'^(?P<slug>[a-zA-Z0-9_.-]+)/$', 'weblog.views.post', name='post'),
    
)