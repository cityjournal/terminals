from weblog.models import Comment
from django import template

register = template.Library()


def get_comments(context, post):
    comments = Comment.objects.filter(post=post)
    context['comments'] = comments
    return ''

register.simple_tag(takes_context=True)(get_comments)