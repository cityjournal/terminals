# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.utils.translation import ugettext, ugettext_lazy as _

from tinymce.widgets import TinyMCE

from weblog.models import Comment, Post

class CommentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            # if isinstance(field.widget, forms.TextInput) or \
            #     isinstance(field.widget, forms.Textarea) or \
            #     isinstance(field.widget, forms.DateInput) or \
            #     isinstance(field.widget, forms.DateTimeInput) or \
            #     isinstance(field.widget, forms.TimeInput):
            field.widget.attrs.update({'class': 'form-control'})
    class Meta:
        model = Comment


class PostForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            # if isinstance(field.widget, forms.TextInput) or \
            #     isinstance(field.widget, forms.Textarea) or \
            #     isinstance(field.widget, forms.DateInput) or \
            #     isinstance(field.widget, forms.DateTimeInput) or \
            #     isinstance(field.widget, forms.TimeInput):
            field.widget.attrs.update({'class': 'form-control'})
    #content = forms.CharField(label=u'Контент', widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
    #intro = forms.CharField(label=u'Анонс', widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
    class Meta:
        model = Post