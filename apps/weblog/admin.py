from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin
from mce_filebrowser.admin import MCEFilebrowserAdmin

from weblog.models import Post, Comment, Tag

class PostAdmin(AdminImageMixin, MCEFilebrowserAdmin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


class CommentAdmin(admin.ModelAdmin):
    pass

class TagAdmin(admin.ModelAdmin):
    pass



admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Tag, TagAdmin)