from django.shortcuts import render_to_response, get_object_or_404, RequestContext, HttpResponseRedirect 
from django.core.paginator import EmptyPage, PageNotAnInteger
from flynsarmy_paginator.paginator import FlynsarmyPaginator
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils import translation

from weblog.models import Post, Comment
from weblog.forms import CommentForm
from main.models import Shop

def index(request):
    ctx = {}
    if 'type' in request.GET:
        posts = Post.objects.filter(type=request.GET['type']).order_by('-updated_at')[:3]
    else:
        posts = Post.objects.filter(type='sales').order_by('-updated_at')[:3]
    # paginator = FlynsarmyPaginator(posts, 6, adjacent_pages=10)
    # page = request.GET.get('page')
    # try:
    #     posts = paginator.page(page)
    # except PageNotAnInteger:
    #     posts = paginator.page(1)
    # except EmptyPage:
    #     posts = paginator.page(paginator.num_pages)
    ctx.update({'posts':posts})
    return render_to_response("weblog/index.html", ctx, context_instance=RequestContext(request))

def shop_posts(request, shop):
    ctx = {}
    shop = get_object_or_404(Shop, slug=shop)
    posts = Post.objects.filter(shop=shop).order_by('updated_at')
    # paginator = FlynsarmyPaginator(posts, 6, adjacent_pages=10)
    # page = request.GET.get('page')
    # try:
    #     posts = paginator.page(page)
    # except PageNotAnInteger:
    #     posts = paginator.page(1)
    # except EmptyPage:
    #     posts = paginator.page(paginator.num_pages)
    ctx.update({'posts':posts})
    return render_to_response("weblog/index.html", ctx, context_instance=RequestContext(request))


def post(request, slug):
    ctx = {}
    post = get_object_or_404(Post, slug=slug)
    comments = Comment.objects.filter(post=post)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('post', args=[post.slug]))
    else:
        form = CommentForm(initial={'post':post})
    ctx.update({'post':post, 'comments':comments, 'form':form})
    return render_to_response('weblog/post.html', ctx, context_instance=RequestContext(request))