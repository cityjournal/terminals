# -*- coding: utf-8 -*-
from django import forms

from .models import Banner

class BannerForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(BannerForm, self).__init__(*args, **kwargs)
		for key, field in self.fields.items():
			field.widget.attrs.update({'class':'form-control'})
	class Meta:
		model = Banner