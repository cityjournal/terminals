# -*- coding: utf-8 -*-
import json

from django.http import HttpResponse, HttpResponseNotFound

from sorl.thumbnail import get_thumbnail

from adapp.models import Banner, Statistic

def get_banner(request):
    if request.method == 'GET' and request.is_ajax():
        # random banner
        banner = Banner.objects.order_by('?')[0]
        im = get_thumbnail(banner.image, '1080x440', crop="center", quality=99)
        try:
            statistic = Statistic.objects.get(banner=banner)
            statistic.shows += 1
            statistic.save()
        except Statistic.DoesNotExist:
            statistic = Statistic(banner=banner)
            statistic.save()
        data = json.dumps({'src':im.url, 'title':banner.title})
        return HttpResponse(data, mimetype='application/json')
    else:
        return HttpResponseNotFound('<h1>Page not found</h1>')
