# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext as _

from sorl.thumbnail import ImageField, get_thumbnail


class Banner(models.Model):
    title = models.CharField(max_length=125)
    slug = models.SlugField(unique=True)
    image = ImageField(upload_to="uploads/banners/", blank=True)
    text = models.TextField(blank=True)

    def __unicode__(self):
        return self.title

class Statistic(models.Model):
    banner = models.ForeignKey(Banner)
    last_show = models.DateTimeField(auto_now=True)
    shows = models.PositiveIntegerField(default=1)


    # def __unicode__(self):
    #     return self.pk