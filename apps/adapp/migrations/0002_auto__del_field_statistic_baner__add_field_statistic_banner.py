# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Statistic.baner'
        db.delete_column(u'adapp_statistic', 'baner_id')

        # Adding field 'Statistic.banner'
        db.add_column(u'adapp_statistic', 'banner',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['adapp.Banner']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Statistic.baner'
        db.add_column(u'adapp_statistic', 'baner',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['adapp.Banner']),
                      keep_default=False)

        # Deleting field 'Statistic.banner'
        db.delete_column(u'adapp_statistic', 'banner_id')


    models = {
        u'adapp.banner': {
            'Meta': {'object_name': 'Banner'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        u'adapp.statistic': {
            'Meta': {'object_name': 'Statistic'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapp.Banner']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_show': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'shows': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['adapp']