# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Banner'
        db.create_table(u'adapp_banner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=125)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'adapp', ['Banner'])

        # Adding model 'Statistic'
        db.create_table(u'adapp_statistic', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('baner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['adapp.Banner'])),
            ('last_show', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('shows', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'adapp', ['Statistic'])


    def backwards(self, orm):
        # Deleting model 'Banner'
        db.delete_table(u'adapp_banner')

        # Deleting model 'Statistic'
        db.delete_table(u'adapp_statistic')


    models = {
        u'adapp.banner': {
            'Meta': {'object_name': 'Banner'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        u'adapp.statistic': {
            'Meta': {'object_name': 'Statistic'},
            'baner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapp.Banner']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_show': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'shows': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['adapp']