from django.contrib import admin

from sorl.thumbnail.admin import AdminImageMixin

from adapp.models import Banner, Statistic

class BannerAdmin(AdminImageMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

class StatisticAdmin(admin.ModelAdmin):
    list_display = ('banner', 'last_show', 'shows')


admin.site.register(Banner, BannerAdmin)
admin.site.register(Statistic, StatisticAdmin)