# encoding: utf-8
from django.conf.urls import patterns, url
from fileupload.views import (
        BasicVersionCreateView, BasicPlusVersionCreateView,
        jQueryVersionCreateView, AngularVersionCreateView,
        PictureCreateView, PictureDeleteView, PictureListView,
        VideoBasicVersionCreateView, VideoBasicPlusVersionCreateView,
        VideoCreateView, VideoDeleteView, VideoListView,
        )

urlpatterns = patterns('',
    url(r'^basic/$', BasicVersionCreateView.as_view(), name='upload-basic'),
    url(r'^basic/plus/$', BasicPlusVersionCreateView.as_view(), name='upload-basic-plus'),
    url(r'^basic/plus/(?P<slug>[a-zA-Z0-9_.-]+)/$', BasicPlusVersionCreateView.as_view(), name='upload-basic-plus'),
    url(r'^new/$', PictureCreateView.as_view(), name='upload-new'),
    url(r'^angular/$', AngularVersionCreateView.as_view(), name='upload-angular'),
    url(r'^jquery-ui/$', jQueryVersionCreateView.as_view(), name='upload-jquery'),
    url(r'^delete/(?P<pk>\d+)$', PictureDeleteView.as_view(), name='upload-delete'),
    url(r'^view/', PictureListView.as_view(), name='upload-view'),
    #url(r'^view/(?P<slug>[a-zA-Z0-9_.-]+)/$', PictureListView.as_view(), name='upload-view'),

    # next for video
    url(r'^video/basic/$', VideoBasicVersionCreateView.as_view(), name='video-upload-basic'),
    url(r'^video/basic/plus/$', VideoBasicPlusVersionCreateView.as_view(), name='video-upload-basic-plus'),
    url(r'^video/basic/plus/(?P<slug>[a-zA-Z0-9_.-]+)/$', VideoBasicPlusVersionCreateView.as_view(), name='video-upload-basic-plus'),
    url(r'^video/new/$', VideoCreateView.as_view(), name='video-upload-new'),
    url(r'^video/delete/(?P<pk>\d+)$', VideoDeleteView.as_view(), name='video-upload-delete'),
    url(r'^video/view/', VideoListView.as_view(), name='video-upload-view'),
)
