# encoding: utf-8
import json

from django.http import HttpResponse
from django.views.generic import CreateView, DeleteView, ListView

from main.models import ShopGallery, Shop, Video, Playlist
from main.forms import ShopGalleryForm, VideoForm
from django.shortcuts import get_object_or_404

from .response import JSONResponse, response_mimetype
from .serialize import serialize, video_serialize



# ==================================== video upload
class VideoCreateView(CreateView):
    model = Video
    form_class = VideoForm

    def get_initial(self):
        if 'slug' in self.kwargs:
            playlist = get_object_or_404(Playlist, slug=self.kwargs['slug'])
            return { 'playlist': playlist }

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(VideoCreateView, self).get_context_data(**kwargs)
        if 'slug' in self.kwargs:
            playlist = get_object_or_404(Playlist, slug=self.kwargs['slug'])
            context['playlist'] = playlist
        return context
    def form_valid(self, form):
        self.object = form.save()
        files = [video_serialize(self.object)]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        data = json.dumps(form.errors)
        return HttpResponse(content=data, status=400, content_type='application/json')

class VideoBasicVersionCreateView(VideoCreateView):
    template_name_suffix = '_video_basic_form'

# class BasicVideoCreateView(VideoCreateView):
#     template_name_suffix = '_video_basic_form'

class VideoBasicPlusVersionCreateView(VideoCreateView):
    template_name_suffix = '_video_basicplus_form'




class VideoDeleteView(DeleteView):
    model = Video

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        response = JSONResponse(True, mimetype=response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class VideoListView(ListView):
    model = Video
    def get_queryset(self, playlist=None):
        if playlist:
            return Video.objects.filter(playlist=playlist)
        else:
            return Video.objects.all()
    def render_to_response(self, context, **response_kwargs):
        if 'playlist' in self.request.GET:
            files = [ video_serialize(p) for p in self.get_queryset(self.request.GET['playlist']) ]
        else:
            files = [ video_serialize(p) for p in self.get_queryset() ]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

# =============================== picture upload

class PictureCreateView(CreateView):
    model = ShopGallery
    form_class = ShopGalleryForm

    def get_initial(self):
        if 'slug' in self.kwargs:
            shop = get_object_or_404(Shop, slug=self.kwargs['slug'])
            return { 'shop': shop }

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PictureCreateView, self).get_context_data(**kwargs)
        if 'slug' in self.kwargs:
            shop = get_object_or_404(Shop, slug=self.kwargs['slug'])
            context['shop'] = shop
        return context
    def form_valid(self, form):
        self.object = form.save()
        files = [serialize(self.object)]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        data = json.dumps(form.errors)
        return HttpResponse(content=data, status=400, content_type='application/json')


class BasicVersionCreateView(PictureCreateView):
    template_name_suffix = '_basic_form'

# class BasicVideoCreateView(VideoCreateView):
#     template_name_suffix = '_video_basic_form'

class BasicPlusVersionCreateView(PictureCreateView):
    template_name_suffix = '_basicplus_form'


class AngularVersionCreateView(PictureCreateView):
    template_name_suffix = '_angular_form'


class jQueryVersionCreateView(PictureCreateView):
    template_name_suffix = '_jquery_form'


class PictureDeleteView(DeleteView):
    model = ShopGallery

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        response = JSONResponse(True, mimetype=response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class PictureListView(ListView):
    model = ShopGallery
    def get_queryset(self, shop=None):
        if shop:
            return ShopGallery.objects.filter(shop=shop)
        else:
            return ShopGallery.objects.all()
    def render_to_response(self, context, **response_kwargs):
        if 'shop' in self.request.GET:
            files = [ serialize(p) for p in self.get_queryset(self.request.GET['shop']) ]
        else:
            files = [ serialize(p) for p in self.get_queryset() ]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response
