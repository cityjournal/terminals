from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
   
   url(r'^(?P<year>\d{4})/(?P<month>\d{2})/$', 'eventcalendar.views.calendar', name='calendar'),
   url(r'^(?P<slug>[a-zA-Z0-9_.-]+)/$', 'eventcalendar.views.event', name='event'),
    
)