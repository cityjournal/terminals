# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from django.core.validators import MaxLengthValidator, MinValueValidator
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField, get_thumbnail
from tinymce.models import HTMLField

from main.models import Shop

class Event(models.Model):
    title = models.CharField(_(u'Название'), max_length=255)
    slug = models.SlugField(_(u'URL'), unique=True)
    image = ImageField(_(u'Картинка'), upload_to="uploads/events/")
    #content = HTMLField(blank=True)
    shop = models.ForeignKey(Shop)
    text = models.TextField(blank=True, validators=[MaxLengthValidator(715)])
    language = models.CharField(_(u'Язык контента'), max_length=15, choices=settings.LANGUAGES, default="ru")
    #start_date = models.DateField()
    #finish_date = models.DateField()
    date = models.DateField()
    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")

    def __unicode__(self):
        return self.title

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '125x125', crop='center')
            return "<img src='%s'>" % im.url
        return ''
    preview.allow_tags = True

    def calendar_thumb(self):
        if self.image:
            im = get_thumbnail(self.image, '140x125', crop='center')
            #return "<img src='%s'>" % im.url
            return im.url
        return ''
    
    def get_absolute_url(self):
        return reverse('event', args=[self.slug])



# class EventGallery(models.Model):
#     title = models.CharField(max_length=200)
#     description = HTMLField(blank=True)
#     event = models.ForeignKey(Event)
#     image = ImageField(upload_to="uploads/events/")
#     language = models.CharField(max_length=15, choices=settings.LANGUAGES, default="ru")
#     class Meta:
#         verbose_name = _("Event image gallery")
#         verbose_name_plural = _("Events images gallery")
    
#     def __unicode__(self):
#         return self.title