from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin
from mce_filebrowser.admin import MCEFilebrowserAdmin

from eventcalendar.models import Event



# class EventImagesInline(AdminImageMixin, admin.TabularInline):
#     fieldsets = (
#         (
#             None, 
#             {
#                 'fields': ('title', 'image',)
#             }
#         ),
#     )

#     model = EventGallery
#     extra = 0


class EventAdmin(AdminImageMixin, MCEFilebrowserAdmin, admin.ModelAdmin):
    # inlines = (EventImagesInline, )
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'preview')

# class EventGalleryAdmin(AdminImageMixin,  MCEFilebrowserAdmin, admin.ModelAdmin):
#     fieldsets = [
#         (None, {'fields': ['title','description', 'shop', 'image']}),
#     ]


admin.site.register(Event, EventAdmin)