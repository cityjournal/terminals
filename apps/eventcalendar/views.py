# -*- coding: utf-8 -*-
from datetime import date

from django.shortcuts import render_to_response, get_object_or_404
from django.utils.safestring import mark_safe
from django.template.context import RequestContext
from django.db.models import Q

from eventcalendar.models import Event
from eventcalendar.custom_calendar import EventCalendar


def calendar(request, year, month):
    year = int(year)
    month = int(month)
    dt = date(year, month, 1)
    # TODO fix february 28 days
    events = Event.objects.order_by('-date').filter(date__lte=date(year, month, 28), date__gte=date(year, month, 1))
    next_month = dt.month + 1
    previous_month = dt.month - 1
    next_dt = date(year, next_month, 1)
    previous_dt = date(year, previous_month, 1)
    cal = EventCalendar(events).formatmonth(year, month)
    ctx = {'calendar': mark_safe(cal), 'previous_dt':previous_dt, 'next_dt':next_dt, 'dt':dt}
    return render_to_response('events.html', ctx, context_instance=RequestContext(request))

def event(request, slug):
    event = get_object_or_404(Event, slug=slug)
    events = Event.objects.order_by('-date').exclude(id=event.id)
    try:
	next_event = events[0]
    except:
	next_event = None
	
    try:
	previous_event = events.reverse()[0]
    except:
	previous_event = None
    current_date = date.today()
    #next_event = Event.objects.filter(date__gt=event.date)
    #previous_event = Event.objects.filter(date__lt=event.date)
    ctx = {'event':event, 'next_event':next_event, 'previous_event':previous_event, 'current_date':current_date}
    return render_to_response('event.html', ctx, context_instance=RequestContext(request))

