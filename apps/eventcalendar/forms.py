# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.utils.translation import ugettext, ugettext_lazy as _
from tinymce.widgets import TinyMCE

from eventcalendar.models import Event



class EventForm(forms.ModelForm):
    #content = forms.CharField(label=u'Контент', widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
    date = forms.DateField(input_formats=('%d-%m-%Y',), widget=forms.DateInput(format='%d-%m-%Y'))
    #finish_date = forms.DateField(input_formats=('%d-%m-%Y',), widget=forms.DateInput(format='%d-%m-%Y'))
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})
    class Meta:
        model = Event
