# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Event.content'
        db.delete_column(u'eventcalendar_event', 'content')

        # Adding field 'Event.text'
        db.add_column(u'eventcalendar_event', 'text',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


        # Changing field 'Event.finish_date'
        db.alter_column(u'eventcalendar_event', 'finish_date', self.gf('django.db.models.fields.DateField')())

        # Changing field 'Event.start_date'
        db.alter_column(u'eventcalendar_event', 'start_date', self.gf('django.db.models.fields.DateField')())

    def backwards(self, orm):
        # Adding field 'Event.content'
        db.add_column(u'eventcalendar_event', 'content',
                      self.gf('tinymce.models.HTMLField')(default='', blank=True),
                      keep_default=False)

        # Deleting field 'Event.text'
        db.delete_column(u'eventcalendar_event', 'text')


        # Changing field 'Event.finish_date'
        db.alter_column(u'eventcalendar_event', 'finish_date', self.gf('django.db.models.fields.DateField')(auto_now_add=True))

        # Changing field 'Event.start_date'
        db.alter_column(u'eventcalendar_event', 'start_date', self.gf('django.db.models.fields.DateField')(auto_now_add=True))

    models = {
        u'eventcalendar.event': {
            'Meta': {'object_name': 'Event'},
            'finish_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'ru'", 'max_length': '15'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['eventcalendar']