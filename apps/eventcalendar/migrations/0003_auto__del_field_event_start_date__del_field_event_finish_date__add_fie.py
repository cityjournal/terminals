# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Event.start_date'
        db.delete_column(u'eventcalendar_event', 'start_date')

        # Deleting field 'Event.finish_date'
        db.delete_column(u'eventcalendar_event', 'finish_date')

        # Adding field 'Event.date'
        db.add_column(u'eventcalendar_event', 'date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 3, 4, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Event.start_date'
        db.add_column(u'eventcalendar_event', 'start_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 3, 4, 0, 0)),
                      keep_default=False)

        # Adding field 'Event.finish_date'
        db.add_column(u'eventcalendar_event', 'finish_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 3, 4, 0, 0)),
                      keep_default=False)

        # Deleting field 'Event.date'
        db.delete_column(u'eventcalendar_event', 'date')


    models = {
        u'eventcalendar.event': {
            'Meta': {'object_name': 'Event'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "'ru'", 'max_length': '15'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['eventcalendar']