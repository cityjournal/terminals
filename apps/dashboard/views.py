# -*- coding: utf-8 -*-
import json

from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
#from django.views.generic import CreateView, DeleteView, ListView
from django.core.paginator import EmptyPage, PageNotAnInteger
from flynsarmy_paginator.paginator import FlynsarmyPaginator
from django.conf import settings
from django.utils import translation

from sorl.thumbnail import get_thumbnail

from main.models import Shop, Category, ShopGallery, Video, Playlist
from weblog.models import Post
from eventcalendar.models import Event
from adapp.models import Banner

from main.forms import ShopForm, CategoryForm, PlaylistForm
from weblog.forms import PostForm
from eventcalendar.forms import EventForm
from adapp.forms import BannerForm


@staff_member_required
def index(request):
    ctx = {}
    return render_to_response('dashboard/index.html', ctx, context_instance=RequestContext(request))

def video_update(request):
    if request.user and request.user.is_authenticated():
        if request.is_ajax() and request.method == 'POST':
            for key, value in request.POST.iteritems():
                try:
                    video = Video.objects.get(id=key)
                    video.order = value
                    video.save()
                    playlist = video.playlist
                except:
                    continue
            #videos = { obj.as_dict() for obj in Video.objects.filter(playlist=playlist).order_by('order') }
            videos = {}
            for obj in Video.objects.filter(playlist=playlist).order_by('order'):
                videos[obj.id] = obj.order
            data = json.dumps({'videos':videos})
            return HttpResponse(data, mimetype='application/json')
        else:
            return HttpResponseNotFound('<h1>Page not found</h1>')
    else:
        return HttpResponse(json.dumps({'success':False}), mimetype='application/json', status=401)

@staff_member_required
def banners(request):
    banners = Banner.objects.all().order_by('-pk')
    ctx = {'banners':banners}
    return render_to_response('dashboard/banners.html', ctx, context_instance=RequestContext(request))


@staff_member_required
def banner_create(request):
    ctx = {}
    if request.method == "POST":
        form = BannerForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            ctx.update({'msg':_('Banner was created')})
            return HttpResponseRedirect(reverse('dashboard.views.banners'))
    else:
        form = BannerForm()
    ctx.update({'form':form,})
    return render_to_response('dashboard/banner_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def banner_update(request, slug):
    ctx = {}
    instance = Banner.objects.get(slug=slug)
    if request.method == "POST":
        form = BannerForm(request.POST, request.FILES, instance=instance)
        if 'delete' in form.data:
            instance.delete()
            ctx.update({'msg':_('Banner was deleted')})
            return HttpResponseRedirect(reverse('dashboard.views.banners'))
        else:
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('dashboard.views.banner_update', args=[instance.slug]))
    else:
        form = BannerForm(instance=instance)
    ctx.update({'form':form, 'banner':instance,})
    return render_to_response('dashboard/banner_update.html', ctx, context_instance=RequestContext(request))


@staff_member_required
def videos(request, slug):
    ctx = {}
    playlist = get_object_or_404(Playlist, slug=slug)
    videos = Video.objects.filter(playlist=playlist).order_by('order')
    ctx.update({'videos':videos})
    return render_to_response('dashboard/videos.html', ctx, context_instance=RequestContext(request))


@staff_member_required
def playlists(request):
    ctx = {}
    playlists = Playlist.objects.all()
    ctx.update({'playlists':playlists})
    return render_to_response('dashboard/playlists.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def playlist_create(request):
    ctx = {}
    if request.method == "POST":
        form = PlaylistForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            ctx.update({'msg':_('Playlist was created')})
            return HttpResponseRedirect(reverse('dashboard.views.playlists'))
    else:
        form = PlaylistForm()
    ctx.update({'form':form,})
    return render_to_response('dashboard/playlist_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def playlist_update(request, slug):
    ctx = {}
    instance = Playlist.objects.get(slug=slug)
    if request.method == "POST":
        form = PlaylistForm(request.POST, request.FILES, instance=instance)
        if 'delete' in form.data:
            instance.delete()
            ctx.update({'msg':_('Playlist was deleted')})
            return HttpResponseRedirect(reverse('dashboard.views.playlists'))
        else:
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('dashboard.views.playlists'))
    else:
        form = PlaylistForm(instance=instance)
    ctx.update({'form':form, 'playlist':instance,})
    return render_to_response('dashboard/playlist_update.html', ctx, context_instance=RequestContext(request))


@staff_member_required
def shops(request):
    ctx = {}
    if 'lang' in request.GET and request.GET['lang'] == 'all':
        ctx.update({'lang':'all'})
        shops = Shop.objects.all()
    else:
        shops = Shop.objects.filter()
    ctx.update({'shops':shops})
    return render_to_response('dashboard/shops.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def shop_create(request):
    ctx = {}
    if request.method == "POST":
        form = ShopForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            ctx.update({'msg':_('Shop was created')})
    else:
        form = ShopForm()
    ctx.update({'form':form,})
    return render_to_response('dashboard/shop_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def shop_update(request, slug):
    ctx = {}
    instance = Shop.objects.get(slug=slug)
    if request.method == "POST":
        form = ShopForm(request.POST, request.FILES, instance=instance)
        if 'delete' in form.data:
            instance.delete()
            ctx.update({'msg':_('Shop was deleted')})
            return HttpResponseRedirect(reverse('dashboard.views.shops'))
        else:
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('dashboard.views.shop_preview', args=[instance.slug]))
    else:
        form = ShopForm(instance=instance)
    ctx.update({'form':form, 'shop':instance,})
    return render_to_response('dashboard/shop_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def shop_preview(request, slug):
    shop = get_object_or_404(Shop, slug=slug)
    images = ShopGallery.objects.filter(shop=shop)
    return render_to_response('dashboard/shop_preview.html', locals(), context_instance=RequestContext(request))

@staff_member_required
def categories(request):
    ctx = {}
    if 'lang' in request.GET and request.GET['lang'] == 'all':
        ctx.update({'lang':'all'})
        categories = Category.objects.all()
    else:
        categories = Category.objects.filter()
    ctx.update({'categories':categories})
    return render_to_response('dashboard/categories.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def category_create(request):
    ctx = {}
    if request.method == "POST":
        form = CategoryForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            ctx.update({'msg':_('Category was created')})
            return HttpResponseRedirect(reverse('dashboard.views.categories'))
    else:
        form = CategoryForm()
    ctx.update({'form':form,})
    return render_to_response('dashboard/post_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def category_update(request, slug):
    ctx = {}
    instance = Category.objects.get(slug=slug)
    if request.method == "POST":
        form =  CategoryForm(request.POST, request.FILES, instance=instance)
        if 'delete' in form.data:
            instance.delete()
            ctx.update({'msg':_('Category was deleted')})
            return HttpResponseRedirect(reverse('dashboard.views.categories'))
        else:
            if form.is_valid():
                form.save()
                ctx.update({'msg':_('Category was updated')})
                return HttpResponseRedirect(reverse('dashboard.views.categories'))
    else:
        form = CategoryForm(instance=instance)
    ctx.update({'form':form, 'category':instance,})
    return render_to_response('dashboard/post_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def posts(request):
    ctx = {}
    if 'lang' in request.GET and request.GET['lang'] == 'all':
        ctx.update({'lang':'all'})
        posts = Post.objects.all()
    else:
        posts = Post.objects.filter()
    ctx.update({'posts':posts})
    return render_to_response('dashboard/posts.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def shop_posts(request, shop):
    ctx = {}
    shop = get_object_or_404(Shop, slug=shop)
    posts = Post.objects.filter(shop=shop).order_by('-updated_at')
    paginator = FlynsarmyPaginator(posts, 6, adjacent_pages=10)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    ctx.update({'posts':posts, 'shop':shop})
    return render_to_response('dashboard/posts.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def post_preview(request, slug):
    ctx = {}
    post = get_object_or_404(Post, slug=slug)
    #comments = Comment.objects.filter(post=post)
    # if request.method == "POST":
    #     form = CommentForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #         return HttpResponseRedirect(reverse('post', args=[post.slug]))
    # else:
    #     form = CommentForm(initial={'post':post})
    ctx.update({'post':post})
    return render_to_response('dashboard/post_preview.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def post_create(request, shop=None):
    ctx = {}
    try:
        shop = Shop.objects.get(slug=shop)
    except:
        shop = None
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save()
            ctx.update({'msg':_('Object was created')})
            return HttpResponseRedirect(reverse('dashboard.views.shop_posts', args=[post.shop.slug]))
    else:
        form = PostForm(initial={'shop':shop, 'author':request.user})
    ctx.update({'form':form,})
    return render_to_response('dashboard/post_update.html', ctx, context_instance=RequestContext(request))


@staff_member_required
def post_update(request, slug):
    ctx = {}
    instance = Post.objects.get(slug=slug)
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES, instance=instance)
        if 'delete' in form.data:
            instance.delete()
            ctx.update({'msg':_('Object was deleted')})
            return HttpResponseRedirect(reverse('dashboard.views.posts'))
        else:
            if form.is_valid():
                form.save()
                ctx.update({'msg':_('Object was updated')})
                return HttpResponseRedirect(reverse('dashboard.views.shop_posts', args=[instance.shop.slug]))
    else:
        form = PostForm(instance=instance)
    ctx.update({'form':form, 'post':instance,})
    return render_to_response('dashboard/post_update.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def events(request):
    ctx = {}
    if 'lang' in request.GET and request.GET['lang'] == 'all':
        ctx.update({'lang':'all'})
        events = Event.objects.all()
    else:
        events = Event.objects.filter()
    ctx.update({'events':events})
    return render_to_response('dashboard/events.html', ctx, context_instance=RequestContext(request))

@staff_member_required
def event_create(request):
    ctx = {}
    if request.method == "POST":
        form = EventForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            ctx.update({'msg':_('Event was created')})
    else:
        form = EventForm()
    ctx.update({'form':form,})
    return render_to_response('dashboard/event_update.html', ctx, context_instance=RequestContext(request))


@staff_member_required
def event_update(request, slug):
    ctx={}
    instance = Event.objects.get(slug=slug)
    if request.method == "POST":
        form = EventForm(request.POST, request.FILES, instance=instance)
        if 'delete' in form.data:
            instance.delete()
            return HttpResponseRedirect(reverse('dashboard.views.events'))
        else:
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('dashboard.views.events'))

    else:
        form = EventForm(instance=instance)
    ctx.update({'form':form, 'event':instance})
    return render_to_response('dashboard/event_update.html', ctx, context_instance=RequestContext(request))