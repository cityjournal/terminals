from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
   
    url(r'^$', 'dashboard.views.index', name='dashboard'),
    # shops
    url(r'^shops/$', 'dashboard.views.shops'),
    url(r'^shops/create/$', 'dashboard.views.shop_create'),
    url(r'^shops/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.shop_update'),
    url(r'^shops/preview/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.shop_preview'),
    # events
    url(r'^events/$', 'dashboard.views.events'),
    url(r'^events/create/$', 'dashboard.views.event_create'),
    url(r'^events/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.event_update'),
    # news and sales
    url(r'^posts/$', 'dashboard.views.posts'),
    url(r'^posts/create/$', 'dashboard.views.post_create'),
    url(r'^posts/create/(?P<shop>[a-zA-Z0-9_.-]+)$', 'dashboard.views.post_create'),
    url(r'^posts/(?P<shop>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.shop_posts'),
    url(r'^posts/edit/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.post_update'),
    url(r'^posts/preview/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.post_preview'),
    # categories
    url(r'^categories/$', 'dashboard.views.categories', name='categories'),
    url(r'^categories/create/$', 'dashboard.views.category_create'),
    url(r'^categories/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.category_update'),
    # playlists
    url(r'^playlists/$', 'dashboard.views.playlists'),
    url(r'^playlists/create/$', 'dashboard.views.playlist_create'),
    url(r'^playlists/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.playlist_update'),
    url(r'^video/update/$', 'dashboard.views.video_update'),
    url(r'^video/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.videos'),
    # banners
    url(r'^banners/$', 'dashboard.views.banners'),
    url(r'^banners/create/$', 'dashboard.views.banner_create'),
    url(r'^banners/(?P<slug>[a-zA-Z0-9_.-]+)/$', 'dashboard.views.banner_update'),
)