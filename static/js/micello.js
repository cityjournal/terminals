$(document).ready(function(){

    

    var key = "7f841d91-a831-4f24-8eac-5c4c57ddd6cd";

    var mapDataObject;
    var aid;
    var mapControl;

    micello.maps.init (key, mapInit);
    function  mapInit() {
        micello.maps.onMapError = onCustomMapError;
        mapControl =  new micello.maps.MapControl('map');
        mapDataObject = mapControl.getMapData();
        
        // set up an override function for map changes so we can place the marker when the map loads
        mapDataObject.mapChanged = onMapChanged; // establish callback function for map changes
        
        mapDataObject.loadCommunity(15); // the location coordinates in this example are based on this community id
    }
    
    function onMapChanged (e) { // all map changes trigger this method, including the inital loading
        if (e.comLoad) { // true on map load
            draw_geometry_overlay();
            add_one_inlay();
            getNavigation();
            // get map data
            $.ajax({url:"/get-map-data/", type:"GET", data:{}, dataType:"json", 
                success:function(response){
                    console.log(response.shops);
                    var shopData = response.shops;
                    buildDirectory(shopData);
                    // var message = response.msg;
                    // $.each(message, function(k, v){
                    //     $.each($('.sims'), function(key, value){
                    //         if ($(value).text() == k){
                    //             // console.log($(value).parent('tr').find('td.balance').text());
                    //             // find td with balance of SIM card and change value
                    //             $(value).parent('tr').find('td.balance').text(v).hide().fadeIn('slow');
                    //         }
                    //     });
                    // });
                }
            });
            
        }
    }
    // for drawing geometry objects
    function draw_geometry_overlay () {

        var currentLevel = mapDataObject.getCurrentLevel();
        var geometry1  =  {
            "lid": currentLevel.id, // level id
            "t": "Search Result", // style
            //"shp": [[0,900.0,250.0],[1,1200.0,250.0],[1,1200.0,400.0],[1,900.0,400.0],[4]],
            "l": [1000.0,225.0,200,200,0], // text label position
            "lr": "Fist project :D" // label text
        };
        mapDataObject.addGeometryOverlay(geometry1);    
        aid = geometry1.aid; 

    }
    // for object hovering efect
    function add_one_inlay () {
        console.log(mapDataObject)
        var inlay = {"id": 3217171, "t": "Search Result", "anm":"my_inlays"};
        mapDataObject.addInlay(inlay);
        aid = inlay.aid; // keep track of the unique annotation id for this inlay
    }

    // custom error handler
    function onCustomMapError (error) {
        alert("The error was "+error);
    }

    // custom nav
    function getNavigation () {
        var level = mapDataObject.getCurrentLevel();
        mapControl.requestNavFromGeom(level.g[0], level.id, true);
        mapControl.requestNavToGeom(level.g[3], level.id, true);
    }

    // this perhaps could override map data of shops
    function buildDirectory(shopData) {
        var community = mapData.getCommunity(); // get the community object
        var floor = community.d[0].l[0]; // default floor, default drawing
        for (var i = 0; i < floor.g.length; i++) {
            for(var j = 0; j < shopData.length; j++){
                // console.log('micello data: ' + floor.g[i].id + ' map data: ' + shopData[j].unit);
                if (floor.g[i].id == shopData[j].unit){ // check shop unit and map data then assign shop to unit on map
                    //console.log(shopData[j].name);
                    floor.g[i].nm = shopData[j].name;
                    floor.g[i].lr = shopData[j].name;
                } 
            }
            if (floor.g[i].nm) {
                if (floor.g[i].nm == "Macy's"){
                    //console.log(floor.g[i].id)
                }
            }
        }
    }

})