//change language (keyboard layout) function 
function ChangeLang(mode)
{
  if (mode >= 4) { 
	  $('#textarea').attr("dir", "rtl");
	  $('.delete_btn').hide();
	  $('.delete_btn2').show();
  } 
  else {
	  $('#textarea').attr("dir", "ltr");
	  $('.delete_btn').show();
	  $('.delete_btn2').hide();
  }
  for (i=1; i<=47; i++)
	  {  
	     $('a[name='+(i)+']').text(chars[mode][i]);
	     $('a[name='+(i)+']').attr('title', chars[mode][i])
	  }
	
}

$(document).ready(function() {

  ChangeLang(2);  //set default language to 0 (english), 4 - hebrew
  $(".form_lang :last").attr("selected", "selected");
  
// click on keyboard button
$('.keyboard_content .key_button').click(function (event) 
{ 
     event.preventDefault(); 
     //here you can also do all sort of things 
     var areaValue = $(this).text();
     if (areaValue == 'Space') areaValue = ' ';
     if (areaValue == 'Пробел') areaValue = ' ';
     if (areaValue == 'СТЕРЕТЬ') areaValue = '';
     if (areaValue == 'ЦИФРЫ И ЗНАКИ' || areaValue == 'БУКВЫ') areaValue = '';
     if (areaValue == 'CTRL' || areaValue == 'Ctrl') areaValue = '';
     if (areaValue == ' Shift' || areaValue == 'БОЛЬШИЕ БУКВЫ') {
         var shift_lang = parseInt($("#lang-switcher").attr('lang'));
         console.log(shift_lang);
         if ($('#shift_opt').is(':checked')) 
         {
           $('#shift_opt').prop('checked', false);
           if (shift_lang == 0){
            shift_lang += 2;
           }
           else{
            shift_lang -=1;
           }
         }
         else
         { 
        	 $('#shift_opt').prop('checked', true);
           if (shift_lang == 0){
        	  shift_lang += 3;
           }
           else{
            shift_lang -=2;
           }
         }   

         ChangeLang(shift_lang);
         areaValue = '';    
     }
     if ($('#shift_opt').is(':checked')) areaValue = areaValue.toUpperCase();
     //insert value at cursor position (little bugged in IE)
     insertAtCaret('textarea', areaValue);
     //insert value to the end of textarea
     //$('#textarea').val($('#textarea').val()+areaValue);
});

// combobox - language select on change
$(".form_lang").change(function() { 
	$('#shift_opt').attr('checked', false);
	ChangeLang($(".form_lang").val()); 

});

//click on backspace button
$('.delete_btn').click(function (event) 
		{ 
	      var areaValue = $('#textarea').val();
	      areaValue = areaValue.substring(0, areaValue.length - 1);
	      $('#textarea').val(areaValue);
		 });

//click on backspace button on hebrew
$('.delete_btn2').click(function (event) 
		{ 
	      var areaValue = $('#textarea').val();
	      areaValue = areaValue.substring(0, areaValue.length - 1);
	      $('#textarea').val(areaValue);
		 });

//click on clear button
$('.keyboard_content .key_clear').click(function (event) { 
	        // $('#textarea').val('');
          var areaValue = $('#textarea').val();
          areaValue = areaValue.substring(0, areaValue.length - 1);
          $('#textarea').val(areaValue);
});

//shit checkbox click
$('#shift_opt').click(function (event) { 
	  if ($('#shift_opt').is(':checked')) 
    {
		  var shift_lang = parseInt($("#lang-switcher").attr('lang'));
	    shift_lang+=3;
	    ChangeLang(shift_lang);
    }
    else
    { 
    	ChangeLang($("#lang-switcher").attr('lang'));
    }  
});

});