$(document).ready(function(){
    $('.ajax-link').click(function(event){
        // console.log('click');
        // console.log(history);
        event.preventDefault();
        // check for back button
        if ($(this).hasClass('back-btm')){
            var url = history[history.length-2];
            history.pop();
        }
        else{
            var url = $(this).attr('url');
            history.push(url);
        }
        $.get( url, function( data ) {
            if (url.indexOf('map') > -1){
                $( "#content" ).html( data );
            }
            else{
                $("#content")
                    .fadeOut(300, function() {
                        $("#content").html(data);
                })
                .fadeIn(300);
            }
        });
    });
});