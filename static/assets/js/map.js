jQuery(document).ready(function($){

    // map bounds
    var southWest = L.latLng(100, -180),
        northEast = L.latLng(-10, 194),
        // northEast = L.latLng(8, 196),
        center = L.latLng(60, 25),
        max_bounds = L.latLngBounds(southWest, northEast);
    

    // level 2 tilelayer
    var level2 = L.tileLayer(STATIC_URL+'img/plan/{level}/{z}/{x}/{y}.png', {
        attribution: 'Startupers',
        'level':'level2',
        tms: true
    });
    
    // level 1 tilelayer
    var level1 = L.tileLayer(STATIC_URL+'img/plan/{level}/{z}/{x}/{y}.png', {
        attribution: 'Startupers',
        'level':'level1',
        tms: true
    });
    // init map
    var map = L.map('map', {
        // center: center,
        minZoom: 2,
        maxZoom: 3,
        zoom: 2,
        maxBounds: max_bounds,
        // layers: [level1, level2]
    }).setView([80, 45], 0);
    level1.addTo(map);
    // map.setMaxBounds(max_bounds);
    // map.invalidateSize();
    // init tilelayers controls
    var levels = {
        '1 уровень':level1,
        '2 уровень':level2,
    };
    var layerControls = L.control.layers(levels, null, {position: 'bottomleft'}).addTo(map);
    
    map.routes1 = [];
    map.routes2 = [];
    map.on('baselayerchange',function(e){
        // console.log('layer changed to', e.name);
        if (e.name === '1 уровень'){
            // show geojson for 1 floor
            geojson_layer.clearLayers();
            geojson_layer.addData(unitDataLevel1);
            map.routes2 = [];
            map.routes2 = routeLayer2.getLayers();
            // show shops for floor 1
            map.removeLayer(shop_titles2);
            shop_titles1.addTo(map);
            // show route for floor 1
            try{
                routeLayer2.clearLayers();
            }
            catch(e){
                console.log('cant clear routeLayer2');
                // map.removeLayer(routeLayer2);
            }
            $.each(map.routes1, function( key, value ) {
                routeLayer1.addLayer(value);
            });
            // check to remove user_marker
            if (user_marker.floor !== 1){
                map.removeLayer(user_marker);
            }
            else{
                user_marker.addTo(map);
            }
        }
        else if (e.name === '2 уровень'){
            // show geojson for 2 floor
            geojson_layer.clearLayers();
            geojson_layer.addData(unitDataLevel2);
            map.routes1 = [];
            map.routes1 = routeLayer1.getLayers();
            // show shops for floor 2
            map.removeLayer(shop_titles1);
            shop_titles2.addTo(map);
            // show route for floor 2
            try{
                routeLayer1.clearLayers();    
            }
            catch(e){
                console.log('cant clear routeLayer1');
                // map.removeLayer(routeLayer1);
            }
            $.each(map.routes2, function( key, value ) {
                routeLayer2.addLayer(value);
            });
            // check to remove user_marker
            if (user_marker.floor !== 2){
                map.removeLayer(user_marker);
            }
            else{
                user_marker.addTo(map);
            }
        }
    });
    // =================================== style controls
    // style zoom controls
    $(".leaflet-top.leaflet-left").removeClass().addClass('leaflet-top leaflet-right custom-zoom');
    // style layer controls
    $(".leaflet-control-layers.leaflet-control").addClass('leaflet-control-layers-expanded custom-layer-control');
    $('.leaflet-bottom.leaflet-left').addClass('custom-control-center');
    // links to category
    var legend = L.control({position: 'topright'});

    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'legend');
        if (typeof cat_type === 'undefined'){
            div.innerHTML="<a href='#' url='/map/' class='active ajax-link'>На карте</a><a href='#' url='/home/' class='ajax-link'>Cписком</a>";  
        }
        else{
            div.innerHTML="<a href='#' url='/map/' class='active ajax-link'>На карте</a><a href='#' url=" + '/' + 'categories/' + cat_type + '/' + cat_slug + '/' + " class='ajax-link'>Cписком</a>";
        }

        return div;
    };

    legend.addTo(map);

    $('.legend.leaflet-control').parent('div').removeClass().addClass('leaflet-top leaflet-left legend-container');

    var position_icon_sm = L.icon({
        iconUrl: STATIC_URL+'img/position_icon.png',
        // iconRetinaUrl: 'my-icon@2x.png',
        iconSize: [30, 33],
        iconAnchor: [0, 0],
        // popupAnchor: [-3, -76],
        // shadowUrl: 'my-icon-shadow.png',
        // shadowRetinaUrl: 'my-icon-shadow@2x.png',
        // shadowSize: [68, 95],
        // shadowAnchor: [22, 94]
    });
    // show current terminal location
    var user_marker = L.marker([0, 0],
            {icon:position_icon_sm}
        ).addTo(map);

    
    // init stairs icon
    var stairs_icon_sm = L.icon({
        iconUrl: STATIC_URL+'img/stairs_icon.png',
        iconSize: [25, 25],
        iconAnchor: [10, 15],
        popupAnchor: [3, -13],
    });
    
    // init stairs markers
    var stairs1_marker = L.marker([0, 0],
            {icon:stairs_icon_sm}
        )
        .bindPopup('<p>Эскалатор</p>')
        .addTo(map);
    var stairs2_marker = L.marker([0, 0],
            {icon:stairs_icon_sm}
        )
        .bindPopup('<p>Эскалатор</p>')
        .addTo(map);

    
    var userPositionButton = L.control({ position: 'bottomright' });
    
    // var userPopupContent = '<b>Вы здесь!</b><br/>Выберите магазин для навигации.';
    // var userPopup = new L.Popup();
    // userPopup.setContent(userPopupContent);
    user_marker.bindPopup('<b>Вы здесь!</b><br/>Выберите магазин для навигации.');

    userPositionButton.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'user-position-button');
        this._div.innerHTML = '<div class="outer-circle"><div class="inner-circle"> </div> </div> ';
        L.DomEvent
                .addListener(this._div, 'click', L.DomEvent.stopPropagation)
                .addListener(this._div, 'click', L.DomEvent.preventDefault)
            .addListener(this._div, 'click', function () { 
                if (user_marker.floor == 1){
                    map.removeLayer(level2);
                    level1.addTo(map);
                }
                else if (user_marker.floor == 2){
                    map.removeLayer(level1);
                    level2.addTo(map);
                }
                user_marker.openPopup(); 
            });
        return this._div;
    };

    userPositionButton.addTo(map);
    
    var excluded_shop_data = history[history.length-1];
    $.ajax({url:"/get-map-data/", type:"GET", data:{'excluded_shop_data':excluded_shop_data}, dataType:"json", 
        success:function(response){
            //console.log(response.shops);
            // var shopData = response.shops;
            // current position icon
            user_marker.setLatLng([response.terminal_xlat, response.terminal_ylat]);
            user_marker.floor = response.terminal_floor;
            // set stairs markers location
            stairs1_marker.setLatLng([response.stairs1_xlat, response.stairs1_ylat]);
            stairs2_marker.setLatLng([response.stairs2_xlat, response.stairs2_ylat]);
            // user_marker.addTo(map);
            //user_marker.bindPopup('<b>Вы здесь!</b><br/>Выберите магазин для навигации.').openPopup();
            // display shop titles
            drawData(response.shops, [response.terminal_xlat, response.terminal_ylat]);
        }
    });


    // change font size according to zoom
    map.on('zoomend', function() {
      var currentZoom = map.getZoom();
      if (currentZoom == 2){
        stairs1_marker.setIcon(stairs_icon_sm);
        stairs2_marker.setIcon(stairs_icon_sm);
        user_marker.setIcon(position_icon_sm);
      }else if(currentZoom == 3){
        var stairs_icon_mid = L.icon({
            iconUrl: STATIC_URL+'img/stairs_icon.png',
            // iconRetinaUrl: 'my-icon@2x.png',
            iconSize: [45, 45],
            iconAnchor: [20, 35],
            popupAnchor: [1, -30],
        });
        var position_icon_mid = L.icon({
            iconUrl: STATIC_URL+'img/position_icon.png',
            // iconRetinaUrl: 'my-icon@2x.png',
            iconSize: [60, 63],
            iconAnchor: [10, 0],
        });
        stairs1_marker.setIcon(stairs_icon_mid);
        stairs2_marker.setIcon(stairs_icon_mid);
        user_marker.setIcon(position_icon_mid);
      }
      else if(currentZoom == 4){
        var stairs_icon_big = L.icon({
            iconUrl: STATIC_URL+'img/stairs_icon.png',
            iconSize: [89, 89],
            iconAnchor: [20, 55],
            popupAnchor: [18, -53],
        });
        var position_icon_big = L.icon({
            iconUrl: STATIC_URL+'img/position_icon.png',
            // iconRetinaUrl: 'my-icon@2x.png',
            iconSize: [86, 95],
            iconAnchor: [-10, -10],
        });
        stairs1_marker.setIcon(stairs_icon_big);
        stairs2_marker.setIcon(stairs_icon_big);
        user_marker.setIcon(position_icon_big);
      }
      //console.log('zoom ', currentZoom);
      $('#map').removeClass('zoom2 zoom3 zoom4');
      $('#map').addClass('zoom'+currentZoom);
    });




    var path = {
        '1':{
            'a':[79.6240562918881, -150.29296875],
            // 'b':[-55.77657, -227.10937],
        },
        '2':{
            'a':[79.6240562918881, -92.373046875],
            // 'b':[54.57206, -501.32812,],
        },
        '3':{
            'a':[79.6240562918881, -55.01953125],
            // 'b':[-55.77657, -227.10937],
        },
        '4':{
            'a':[79.6240562918881, 41.66015625],
            // 'b':[56.55948, 128.67188],
        },
        '5':{
            'a':[45.82879925192134, 41.66015625],
            // 'b':[-56.17002, 136.40625],
        },
        '6':{
            'a':[45.82879925192134, -54.31640625],
            // 'b':[-56.17002, 136.40625],
        },
        '7':{
            'a':[45.82879925192134, -90.52734374999999],
        },
        '8':{
            'a':[45.82879925192134, -148.88671874999997],
            // 'b':[-56.17002, 136.40625],
        },
    }



    // function setWeights(arr) {

    //     $.each(arr, function( key, value ) {
    //         //console.log('key' + key + 'value' + value);
    //         var loc1 = new L.LatLng(value['a'][0], value['a'][1]);
    //         var loc2 = new L.LatLng(value['b'][0], value['b'][1]);
    //         value['weight'] = (loc2.distanceTo(loc1)/1000).toFixed(0);
    //     });
    // }

    function clearMap(event) {
        if (typeof event !== 'undefined' && typeof event !== null){
            event.preventDefault();
        }
        map.routes1 = [];
        map.routes2 = [];
        try{
            routeLayer1.clearLayers();
        }
        catch(e){
            // map.removeLayer(routeLayer2);
            console.log('cant clear map with route 1');
            routeLayer1 = L.layerGroup().addTo(map);
        }
        try{
            routeLayer2.clearLayers();
        }
        catch(e){
            console.log('cant clear map with route 2');
            routeLayer2 = L.layerGroup().addTo(map);

        }
    }

    var navs = {};
    function drawMarkers(arr, navs){

        $.each(arr, function( key, value ) {
            //console.log('key' + key + 'value' + value)
            var loc1 = new L.LatLng(value['a'][0], value['a'][1]);
            var marker = L.marker(loc1).addTo(map);

            var container = $('<div/ >');
            
            // bind for navigation links
            container.on('click', '.navigate', {navs:navs}, navRequest);
            // bind for clear map link
            container.on('click', '#clearMap', {navs:navs}, clearMap);

            container.html(
                "<b>"+ "Node "+ key + " Location: " + "</b>" + loc1 + "<br/>" +
                '<a href="#" class="navigate" nav="from" node="' + key + '">Navigate from</a><br>' + 
                '<a href="#" class="navigate" nav="to" node="' + key + '">Navigate to</a><br>' +
                '<a href="#" id="clearMap">Clear route</a><br>'
            );
            
            marker.bindPopup(container[0]);
        });
    }
    // setWeights(path)
    // drawMarkers(path, navs);

    function navRequest(e){
        e.preventDefault();

        // set navigation point role. could be 'from' or 'to' or 'unit'
        if ( $(this).attr('node') ){
            // console.log('node');
            e.data.navs[$(this).attr('nav')] = $(this).attr('node');
        }
        else if ( $(this).attr('unit') ){
            // console.log('unit');
            e.data.navs['unit'] = $(this).attr('unit');
        }
        // if array has 'from' and 'to' nodes make AJAX request to server
        if ( 'to' in e.data.navs || 'unit' in e.data.navs) {
            console.log(e.data.navs);
            clearMap();
            $.ajax({url:"/navigate/", type:"GET", data:e.data.navs, dataType:"json", 
                success:function(response){
                    console.log(response);
                    drawRoute(response);
                }
            });
        }
    }
    



    function drawRoute(route){
        var loc1,
            loc2,
            msg,
            i = 0;

        function recurs() {
            loc1 = route.coord[route.path[i]];
            
            if (route.path[i] == 'stairs1' || route.path[i] == 'stairs2'){
                console.log('stairs in route');
                // draw route on shop's floor
                loc2 = route.coord[route.path[i+1]];
                loc1 = route.coord['stairs1'] || route.coord['stairs2'];
                createPolyLine(loc1, loc2, 2);
                // clear route on another floor
                // clearMap();
                // show shop's floor tiles
                if (route.coord['unit'][2] > route.coord['terminal'][2]){
                    map.removeLayer(level1);
                    level2.addTo(map);
                    // level2.bringToFront();
                }
                else if (route.coord['unit'][2] < route.coord['terminal'][2]){
                    map.removeLayer(level2);
                    level1.addTo(map);
                    // level1.bringToFront();
                }
            }       
            else if (route.path[i+1] == 'unit' && route.path.length > 2){
                // console.log('unit in route');
                // make curve exactly before shop unit
                // console.log('now', route.coord['unit'][0], loc1[1]);
                // console.log('before', loc1[0], route.coord['unit'][1]);
                // console.log('loc1', loc1)
                // console.log('unit', route.coord['unit']);
                if (route.coord['unit'][0] < loc1[0] && route.coord['unit'][1] > loc1[1]){
                    loc2 = [route.coord['unit'][0], loc1[1]];
                }
                else if (route.coord['unit'][0] > loc1[0] && route.coord['unit'][1] < loc1[1]){
                    loc2 = [loc1[0], route.coord['unit'][1]];
                }
                else if (route.coord['unit'][0] < loc1[0] && route.coord['unit'][1] < loc1[1]){
                    loc2 = [loc1[0], route.coord['unit'][1]];
                }
                // loc2 = [route.coord['unit'][0], loc1[1]];
                // loc2 = [route.coord['unit'][0], route.coord['unit'][1]];
                //console.log(loc2);
                createPolyLine(loc1, loc2);
                loc1 = loc2;
                loc2 = route.coord['unit'];
                createPolyLine(loc1, loc2);
                //console.log(loc2);
            }
            else {
                // console.log('no unit in route');
                loc2 = route.coord[route.path[i+1]];
                createPolyLine(loc1, loc2);
            }
            i++;
            if (i < route.path.length-1 && route.path[i] == 'stairs1' || route.path[i] == 'stairs2') {
                if (route.coord['unit'][2] > route.coord['terminal'][2]){
                    // msg = '<p>Здесь поднимитесь<br/>на второй этаж</p>';
                    map.removeLayer(level2);
                    level1.addTo(map);
                    // level1.bringToFront();
                }
                else if (route.coord['unit'][2] < route.coord['terminal'][2]){
                    // msg = '<p>Здесь спуститесь<br/>на первый этаж</p>';
                    map.removeLayer(level1);
                    level2.addTo(map);
                    //level2.bringToFront();
                }
                // show message to user
                if (route.path[i] == 'stairs1'){
                    stairs1_marker.openPopup();
                }
                else if (route.path[i] == 'stairs2'){
                    stairs2_marker.openPopup();
                }
                // var stairs_loc = route.coord['stairs1'] || route.coord['stairs2'];
                // var popup = L.popup()
                //     .setLatLng(stairs_loc)
                //     .setContent('<p>Воспользуйтесь<br/>эскалатором</p>')
                //     .openOn(map);

                setTimeout(recurs, 3000);
            }
            else if(i < route.path.length-1 && route.path[i] != 'stairs1' || route.path[i] != 'stairs2'){recurs()}

        }
        recurs();
    }   
    
    
    map.on('click', function(e) {
        console.log( e.latlng.lng + " " + e.latlng.lat + "," );
    });



    //draw all the data about shops on the map
    var shop_titles1 = L.layerGroup().addTo(map);
    var shop_titles2 = L.layerGroup();
    function drawData(shopData, terminal_coord) {
        for(var i = 0; i < shopData.length; i++){
            if (shopData[i].iconUrl){
                var shopIcon = L.icon({
                    iconUrl: shopData[i].icon_url,
                    // iconRetinaUrl: 'my-icon@2x.png',
                    iconSize: shopData[i].icon_size,
                    iconAnchor: [0, 0],
                    // popupAnchor: [-3, -76],
                    // shadowUrl: 'my-icon-shadow.png',
                    // shadowRetinaUrl: 'my-icon-shadow@2x.png',
                    // shadowSize: [68, 95],
                    // shadowAnchor: [22, 94]
                });
                
                var marker = L.marker(
                    [shopData[i].xlat, shopData[i].ylat],
                    {icon:shopIcon}).addTo(map);
            }
            else{   
                var html_content;
                if (shopData[i].rotated_title === true){
                    html_content = "<div class='rotated-title'" + "style='font-size:" +shopData[i].font_size+"px;'>" + shopData[i].content + "</div>";
                }
                else{
                    // html_content = shopData[i].content;
                    html_content = "<div" + "style='font-size:" +shopData[i].font_size+"px;'>" + shopData[i].content + "</div>";
                }
                
                var shopTitle = L.divIcon({
                        className: 'map-shop-title grey', 
                        //html:'<div style="-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);-o-transform: rotate(-90deg);-ms-transform: rotate(-90deg);transform: rotate(-90deg);">' + shopData[i].name+'</div>',
                        html:html_content,
                    });
                var marker = L.marker(
                        [shopData[i].title_x, shopData[i].title_y],
                        {icon:shopTitle}
                    );
                marker.unit_id = shopData[i].unit_id;
                marker.floor = shopData[i].floor;
                marker.features = shopData[i].features;
                if (shopData[i].floor == 1){
                    shop_titles1.addLayer(marker);
                }
                else if(shopData[i].floor == 2){
                    shop_titles2.addLayer(marker);
                }
                marker.on('click', function(e) {
                    $.each( $( ".map-shop-title.white" ), function() {
                        $(this).removeClass('white').addClass('grey');
                    });
                    $(e.target._icon).removeClass('grey').addClass('white');
                    geojson_layer.clearLayers();
                    geojson_layer.addData(e.target.features);
    
                    // bounds to shop and terminal
                    map.fitBounds([e.target._latlng, terminal_coord]);
                });
            }

            var container = $('<div/ >');
            
            // bind for navigation links
            container.on('click', '.navigate', {navs:navs}, navRequest);
            // bind for clear map link
            container.on('click', '.shop-link', {navs:navs}, showShop);

            container.html(
                // "<b>"+ shopData[i].name + "</b>" + "<br/>" + shopData[i].info + "<br>" +
                "<div class='shop-title'>"+ shopData[i].name + "</div>" +
                '<a href="#" class="navigate" nav="to" unit="' + shopData[i].unit_id + '"></a>' +
                '<a href="#" class="shop-link" url="'+ shopData[i].shop_url +'"></a>'

            );
            marker.bindPopup(container[0]);
            //draw markers for all items
            var loc = new L.LatLng(shopData[i].xlat, shopData[i].ylat);
            // createPolyLine(loc, userLocation);

        }
    }

    //draw polyline between two locations
    var routeLayer1 = L.layerGroup().addTo(map);
    var routeLayer2 = L.layerGroup().addTo(map);
    function createPolyLine(loc1, loc2, floor) {
        var latlongs = [loc1, loc2];
        floor = typeof floor !== 'undefined' ? floor : 1;
        // if (Math.abs(loc1.lng - loc2.lng) > 180) {
        //     latlongs = [loc1.wrap(179, -179), loc2];
        // }
        var polyline = new L.Polyline(latlongs, {
            color: '#fdf300',
            opacity: 1,
            weight: 4,
            clickable: false
        });
        // var decorator = L.polylineDecorator(polyline, {
        //         patterns: [
        //             // define a pattern of 10px-wide dashes, repeated every 20px on the line 
        //             {offset: 0, repeat: '10px', symbol: new L.Symbol.dash({pixelSize: 2, pathOptions: {stroke: true, weight: 4, color:'white'} })}
        //         ]
        //     });
        // routeLayer.addLayer(polyline);
        // routeLayer.addLayer(decorator);
        // routeLayer.addTo(map);
        if (floor == 1 && routeLayer2.getLayers().length == 0){
            routeLayer1.addLayer(polyline);
            console.log('draw on route 1');
        }
        else if (floor == 2){
            routeLayer2.addLayer(polyline);
            console.log('draw on route 2');
        }
        else{
            routeLayer2.addLayer(polyline);
            console.log('draw on route 2', routeLayer2.getLayers());
        }
    }


    


    //=============================== geojson stuff start here
    var geojson_level1,
        geojson_level2;
    // styling geojson
    function style(feature) {
        return {
            // weight: 0,
            // opacity: 0,
            // color: 'transparent',
            // fillColor: 'transparent',
            // fillOpacity: 0
            
            weight: 3,
            opacity: 1,
            color: '#3ca5a9',
            fillColor: '#3ca5a9',
            fillOpacity: 1
        };
    }
    
    // highlight feature
    function highlightFeature(e) {
        var layer = e.target;
        layer.marker.openPopup();
        layer.setStyle({
            weight: 3,
            opacity: 1,
            color: '#3ca5a9',
            // dashArray: '3',
            fillColor: '#3ca5a9',
            fillOpacity: 1
        });

        if (!L.Browser.ie && !L.Browser.opera) {
            layer.bringToFront();
        }
    }
    
    // reset highlight
    function resetHighlight(e) {
        geojson.resetStyle(e.target);
    }

    function showShop(event){
        event.preventDefault();
        console.log("show shop");
        var url = $(this).attr('url');
        history.push(url);
        $.get( url, function( data ) {
            if (url.indexOf('map') > -1){
                $( "#content" ).html( data );
            }
            else{
                $("#content")
                    .fadeOut(300, function() {
                        $("#content").html(data);
                })
                .fadeIn(300);
            }
        });
    }
    
    // handle each geojson object
    function onEachFeature(feature, layer) {
        layer.on({
                // mouseover: highlightFeature,
                // mouseout: resetHighlight,
                click: highlightFeature
            });
        var container = $('<div/ >');
        
        // bind for navigation links
        container.on('click', '.navigate', {navs:navs}, navRequest);
        // bind for clear map link
        container.on('click', '.shop-link', {navs:navs}, showShop);

        container.html(
            "<div class='shop-title'>"+ feature.name + "</div>" +
            '<a href="#" class="navigate" nav="to" unit="' + feature.id + '"></a>' +
            '<a href="#" class="shop-link" url="'+ feature.shop_url +'"></a>'
            // "<b>"+ shopData[i].name + "</b>" + "<br/>" + shopData[i].info + "<br>" +
            // "<b>"+ feature.name + "</b>" + "<br/>" +
            // '<a href="#" class="navigate" nav="to" unit="' + feature.id + '">Как пройти</a><br>' +
            // '<a href="#" id="clearMap">Очистить маршрут</a><br>'
        );
        if (feature.title_x){
            var html_content;
            if (feature.rotated_title === true){
                // html_content = "<div class='rotated-title'>" + feature.content + "</div>";
                html_content = "<div class='rotated-title'" + "style='font-size:" +feature.font_size+"px;'>" + feature.content + "</div>";
            }
            else{
                // html_content = feature.content;
                html_content = "<div " + "style='font-size:" +feature.font_size+"px;'>" + feature.content + "</div>";
            }
            
            var shopTitle = L.divIcon({
                    className: 'map-shop-title', 
                    //html:'<div style="-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);-o-transform: rotate(-90deg);-ms-transform: rotate(-90deg);transform: rotate(-90deg);">' + shopData[i].name+'</div>',
                    html:html_content,
                });
            if (feature.floor == 1){
                var marker = L.marker(
                        [feature.title_x, feature.title_y],
                        {icon:shopTitle}
                    ).addTo(shop_titles1);
            }else if(feature.floor == 2){
                var marker = L.marker(
                        [feature.title_x, feature.title_y],
                        {icon:shopTitle}
                    ).addTo(shop_titles2);
            }

            marker.on('click', function(e) {
                layer.setStyle({
                    weight: 3,
                    opacity: 1,
                    color: '#3ca5a9',
                    // dashArray: '3',
                    fillColor: '#3ca5a9',
                    fillOpacity: 1
                });
            });
            marker.bindPopup(container[0]);
            layer.marker = marker; 
        }

    }
    

    // get geojson for 1 floor

    // var geojson_level1 = L.geoJson([unitDataLevel1], {

    //     style: style,
    //     onEachFeature: onEachFeature,

    // });

    // var geojson_level2 = L.geoJson([unitDataLevel2], {

    //     style: style,
    //     onEachFeature: onEachFeature,

    // });
    var geojson_layer = L.geoJson([unitDataLevel1], {

        style: style,
        onEachFeature: onEachFeature,

    }).addTo(map);
   // L.Util.falseFn(el.offsetWidth); 
});