  chars = new Array(7);
  //english lower case
  chars[0]=new Array(47);
  chars[0][1]="\\";
  chars[0][2]="=";
  chars[0][3]="-";
  chars[0][4]="0";
  chars[0][5]="9";
  chars[0][6]="8";
  chars[0][7]="7";
  chars[0][8]="6";
  chars[0][9]="5";
  chars[0][10]="4";
  chars[0][11]="3";
  chars[0][12]="2";
  chars[0][13]="1";
  chars[0][14]="`";
  
  chars[0][15]="]";
  chars[0][16]="[";
  chars[0][17]="p";
  chars[0][18]="o";
  chars[0][19]="i";
  chars[0][20]="u";
  chars[0][21]="y";
  chars[0][22]="t";
  chars[0][23]="r";
  chars[0][24]="e";
  chars[0][25]="w";
  chars[0][26]="q";
  
  chars[0][27]="'";
  chars[0][28]=";";
  chars[0][29]="l";
  chars[0][30]="k";
  chars[0][31]="j";
  chars[0][32]="h";
  chars[0][33]="g";
  chars[0][34]="f";
  chars[0][35]="d";
  chars[0][36]="s";
  chars[0][37]="a";
  
  chars[0][38]="/";
  chars[0][39]=".";
  chars[0][40]=",";
  chars[0][41]="m";
  chars[0][42]="n";
  chars[0][43]="b";
  chars[0][44]="v";
  chars[0][45]="c";
  chars[0][46]="x";
  chars[0][47]="z";

  //english upper case
  chars[1]=new Array(47);
  chars[1][1]="|";
  chars[1][2]="+";
  chars[1][3]="_";
  chars[1][4]=")";
  chars[1][5]="(";
  chars[1][6]="*";
  chars[1][7]="&";
  chars[1][8]="^";
  chars[1][9]="%";
  chars[1][10]="$";
  chars[1][11]="#";
  chars[1][12]="@";
  chars[1][13]="!";
  chars[1][14]="~";
  
  chars[1][15]="}";
  chars[1][16]="{";
  chars[1][17]="P";
  chars[1][18]="O";
  chars[1][19]="I";
  chars[1][20]="U";
  chars[1][21]="Y";
  chars[1][22]="T";
  chars[1][23]="R";
  chars[1][24]="E";
  chars[1][25]="W";
  chars[1][26]="Q";
  
  chars[1][27]="\"";
  chars[1][28]=":";
  chars[1][29]="L";
  chars[1][30]="K";
  chars[1][31]="J";
  chars[1][32]="H";
  chars[1][33]="G";
  chars[1][34]="F";
  chars[1][35]="D";
  chars[1][36]="S";
  chars[1][37]="A";
  
  chars[1][38]="?";
  chars[1][39]=">";
  chars[1][40]="<";
  chars[1][41]="M";
  chars[1][42]="N";
  chars[1][43]="B";
  chars[1][44]="V";
  chars[1][45]="C";
  chars[1][46]="X";
  chars[1][47]="Z";  

  chars[2]=new Array();

  chars[2][1]="\\";
  chars[2][2]="=";
  chars[2][3]="-";
  chars[2][4]="0";
  chars[2][5]="9";
  chars[2][6]="8";
  chars[2][7]="7";
  chars[2][8]="6";
  chars[2][9]="5";
  chars[2][10]="4";
  chars[2][11]="3";
  chars[2][12]="2";
  chars[2][13]="1";
  chars[2][14]="ё";
  
  chars[2][15]="ъ";
  chars[2][16]="х";
  chars[2][17]="з";
  chars[2][18]="щ";
  chars[2][19]="ш";
  chars[2][20]="г";
  chars[2][21]="н";
  chars[2][22]="е";
  chars[2][23]="к";
  chars[2][24]="у";
  chars[2][25]="ц";
  chars[2][26]="й";
  
  chars[2][27]="э";
  chars[2][28]="ж";
  chars[2][29]="д";
  chars[2][30]="л";
  chars[2][31]="о";
  chars[2][32]="р";
  chars[2][33]="п";
  chars[2][34]="а";
  chars[2][35]="в";
  chars[2][36]="ы";
  chars[2][37]="ф";
  
  chars[2][38]=".";
  chars[2][39]="ю";
  chars[2][40]="б";
  chars[2][41]="ь";
  chars[2][42]="т";
  chars[2][43]="и";
  chars[2][44]="м";
  chars[2][45]="с";
  chars[2][46]="ч";
  chars[2][47]="я";
  
  chars[3]=new Array();

  chars[3][1]="/";
  chars[3][2]="+";
  chars[3][3]="_";
  chars[3][4]=")";
  chars[3][5]="(";
  chars[3][6]="*";
  chars[3][7]="?";
  chars[3][8]=":";
  chars[3][9]="%";
  chars[3][10]=";";
  chars[3][11]="№";
  chars[3][12]="\"";
  chars[3][13]="!";
  chars[3][14]="Ё";
  
  chars[3][15]="Ъ";
  chars[3][16]="Х";
  chars[3][17]="З";
  chars[3][18]="Щ";
  chars[3][19]="Ш";
  chars[3][20]="Г";
  chars[3][21]="Н";
  chars[3][22]="Е";
  chars[3][23]="К";
  chars[3][24]="У";
  chars[3][25]="Ц";
  chars[3][26]="Й";
  
  chars[3][27]="Э";
  chars[3][28]="Ж";
  chars[3][29]="Д";
  chars[3][30]="Л";
  chars[3][31]="О";
  chars[3][32]="Р";
  chars[3][33]="П";
  chars[3][34]="А";
  chars[3][35]="В";
  chars[3][36]="Ы";
  chars[3][37]="Ф";
  
  chars[3][38]=",";
  chars[3][39]="Ю";
  chars[3][40]="Б";
  chars[3][41]="Ь";
  chars[3][42]="Т";
  chars[3][43]="И";
  chars[3][44]="М";
  chars[3][45]="С";
  chars[3][46]="Ч";
  chars[3][47]="Я";  

  chars[4]=new Array();

  chars[4][1]="\\";
  chars[4][2]="=";
  chars[4][3]="-";
  chars[4][4]="0";
  chars[4][5]="9";
  chars[4][6]="8";
  chars[4][7]="7";
  chars[4][8]="6";
  chars[4][9]="5";
  chars[4][10]="4";
  chars[4][11]="3";
  chars[4][12]="2";
  chars[4][13]="1";
  chars[4][14]="`";
  
  chars[4][15]="]";
  chars[4][16]="[";
  chars[4][17]=String.fromCharCode(1508); //ф
  chars[4][18]=String.fromCharCode(1501); //н
  chars[4][19]=String.fromCharCode(1503); //п
  chars[4][20]=String.fromCharCode(1493); //е
  chars[4][21]=String.fromCharCode(1496); //и
  chars[4][22]=String.fromCharCode(1488); //а
  chars[4][23]=String.fromCharCode(1512); //ш
  chars[4][24]=String.fromCharCode(1511); //ч
  chars[4][25]="'";
  chars[4][26]="/";
 
  chars[4][27]=",";
  chars[4][28]=String.fromCharCode(1507); //у
  chars[4][29]=String.fromCharCode(1498); //к
  chars[4][30]=String.fromCharCode(1500); //м
  chars[4][31]=String.fromCharCode(1495); //з
  chars[4][32]=String.fromCharCode(1497); //й
  chars[4][33]=String.fromCharCode(1506); //т
  chars[4][34]=String.fromCharCode(1499); //л
  chars[4][35]=String.fromCharCode(1490); //в
  chars[4][36]=String.fromCharCode(1491); //г
  chars[4][37]=String.fromCharCode(1513); //щ
  
  chars[4][38]=".";
  chars[4][39]=String.fromCharCode(1509); //х
  chars[4][40]=String.fromCharCode(1514); //ъ
  chars[4][41]=String.fromCharCode(1510); //ц
  chars[4][42]=String.fromCharCode(1502); //о
  chars[4][43]=String.fromCharCode(1504); //р
  chars[4][44]=String.fromCharCode(1492); //д
  chars[4][45]=String.fromCharCode(1489); //б
  chars[4][46]=String.fromCharCode(1505); //с
  chars[4][47]=String.fromCharCode(1494); //ж  
  
  
  chars[5]=new Array();

  chars[5][1]="|";
  chars[5][2]="+";
  chars[5][3]="_";
  chars[5][4]=")";
  chars[5][5]="(";
  chars[5][6]="*";
  chars[5][7]="&";
  chars[5][8]="^";
  chars[5][9]="%";
  chars[5][10]="$";
  chars[5][11]="#";
  chars[5][12]="@";
  chars[5][13]="!";
  chars[5][14]="~";
  
  chars[5][15]="}";
  chars[5][16]="{";
  chars[5][17]=String.fromCharCode(1508); //ф
  chars[5][18]=String.fromCharCode(1501); //н
  chars[5][19]=String.fromCharCode(1503); //п
  chars[5][20]=String.fromCharCode(1493); //е
  chars[5][21]=String.fromCharCode(1496); //и
  chars[5][22]=String.fromCharCode(1488); //а
  chars[5][23]=String.fromCharCode(1512); //ш
  chars[5][24]=String.fromCharCode(1511); //ч
  chars[5][25]="'";
  chars[5][26]="/";
 
  chars[5][27]="\"";
  chars[5][28]=":";
  chars[5][29]=String.fromCharCode(1498); //к
  chars[5][30]=String.fromCharCode(1500); //м
  chars[5][31]=String.fromCharCode(1495); //з
  chars[5][32]=String.fromCharCode(1497); //й
  chars[5][33]=String.fromCharCode(1506); //т
  chars[5][34]=String.fromCharCode(1499); //л
  chars[5][35]=String.fromCharCode(1490); //в
  chars[5][36]=String.fromCharCode(1491); //г
  chars[5][37]=String.fromCharCode(1513); //щ
  
  chars[5][38]="?";
  chars[5][39]=">";
  chars[5][40]="<";
  chars[5][41]=String.fromCharCode(1510); //ц
  chars[5][42]=String.fromCharCode(1502); //о
  chars[5][43]=String.fromCharCode(1504); //р
  chars[5][44]=String.fromCharCode(1492); //д
  chars[5][45]=String.fromCharCode(1489); //б
  chars[5][46]=String.fromCharCode(1505); //с
  chars[5][47]=String.fromCharCode(1494); //ж


  chars[6]=new Array();
  chars[6][15]="+";
  chars[6][16]=")";
  chars[6][17]="(";
  chars[6][18]="&";
  chars[6][19]="%";
  chars[6][20]="$";
  chars[6][21]="#";
  chars[6][22]="@";
  chars[6][23]="!";
  
  chars[6][24]="}";
  chars[6][25]="{";
  chars[6][26]="/";
  
  chars[6][27]=":";
  chars[6][28]="?";
  chars[6][29]=">";
  chars[6][30]="<";
  
  chars[6][31]="/";
  chars[6][32]="+";
  chars[6][33]=")";
  chars[6][34]="(";
  chars[6][35]="№";

  chars[6][36]="=";
  chars[6][37]="-";
  chars[6][38]="0";
  chars[6][39]="9";
  chars[6][40]="8";
  chars[6][41]="7";
  chars[6][42]="6";
  chars[6][43]="5";
  chars[6][44]="4";
  chars[6][45]="3";
  chars[6][46]="2";
  chars[6][47]="1";