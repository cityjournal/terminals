# -*- coding: utf-8 -*-
import os, sys
from django.utils.translation import ugettext

gettext = lambda s: s
PROJECT_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(PROJECT_DIR, '../apps'),)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Ros', 'ros@cityjournal.se'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.contrib.gis.db.backends.spatialite', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': PROJECT_DIR + '/../db.sqlite3',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Kiev'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru'
ugettext = lambda s: s
LANGUAGES = (
    ('en', ugettext('English')),
    ('uk', ugettext('Ukrainian')),
    ('ru', ugettext('Russian')),
)
LOCALE_PATHS=[PROJECT_DIR + '/../locale',]

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = PROJECT_DIR + '/../media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = PROJECT_DIR + '/../static/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    'static/assets',
    'static/assets/bootstrap'
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n$9#ps75s&zi!-0bx5tf0hd9tq6+=u3zvufj#l3in5cvg(@zw_'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
)

ROOT_URLCONF = 'terminals.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'terminals.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PROJECT_DIR + '/../templates',
)

TINYMCE_DEFAULT_CONFIG = {
    'theme': "advanced",
    'theme_advanced_buttons1' : "bold,italic,underline,separator,link,unlink,separator,undo,redo,separator,bullist,numlist,separator,fontsizeselect,forecolor,separator,paste,pastetext,pasteword,removeformat,code,image,tablecontrols",
    #'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
    'plugins':'table',
    'file_browser_callback': 'mce_filebrowser',
    'height':350,
}

THUMBNAIL_FORMAT = 'PNG'

PROJECT_APPS = (
    'main',
    'weblog',
    'dashboard',
    'qrcode',
    'eventcalendar',
    'adapp',
    )

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.gis',
    'debug_toolbar',
    'south',
    'tinymce',
    'mce_filebrowser',
    'sorl.thumbnail',
    'rosetta',
    'fileupload',
    'haystack',
)+PROJECT_APPS


#ROSETTA_STORAGE_CLASS = 'rosetta.storage.SessionRosettaStorage'
#ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
ROSETTA_REQUIRES_AUTH = True

INTERNAL_IPS = ('127.0.0.1',)
DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)
DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TEMPLATE_CONTEXT': True,
    'INTERCEPT_REDIRECTS': False,
}

# stairs location
STAIRS1_LOC = [79.6240562918881, 6.85546875]
STAIRS2_LOC = [42.82879925192134, 6.85546875]
#45.82879925192134, 6.85546875

# navigation configuration(plan of the building)
CORNERS = [
    # first floor
    {'node':'1', 'loc':[79.6240562918881, -150.29296875], 'edges':[['2', 1.5], ['8', 2], ]},
    {'node':'2', 'loc':[79.6240562918881, -92.373046875], 'edges':[['1', 1.5], ['3', 0.5], ['7', 2], ]},
    {'node':'3', 'loc':[79.6240562918881, -55.01953125], 'edges':[['2', 0.5], ['4', 2], ['6', 2], ['stairs1', 1.5], ]},
    {'node':'4', 'loc':[79.6240562918881, 41.66015625], 'edges':[['3', 2],  ['5', 2], ['stairs1', 0.5]]},
    {'node':'5', 'loc':[45.82879925192134, 41.66015625], 'edges':[['4', 2], ['6', 2], ['stairs2', 0.5]]},
    {'node':'6', 'loc':[45.82879925192134, -54.31640625], 'edges':[['5', 2], ['3', 2], ['7', 0.5], ['stairs2', 1.5]]},
    {'node':'7', 'loc':[45.82879925192134, -90.52734374999999], 'edges':[['2', 2], ['6', 0.5], ['8', 1.5]]},
    {'node':'8', 'loc':[45.82879925192134, -148.88671874999997], 'edges':[['7', 1.5], ['1', 2]]},
    # stairs
    #4{'node':'stairs1', 'loc': STAIRS1_LOC, 'edges':[['3', 1.5], ['4', 0.5], ['9', 1], ['10', 1]]},
    #{'node':'stairs2', 'loc': STAIRS2_LOC, 'edges':[['5', 0.5], ['6', 1.5], ['11', 1], ['12', 1], ]},
    # second floor
    {'node':'9', 'loc':[79.6240562919, -75.41015624999999], 'edges':[['10', 2], ['12', 1], ['stairs1', 1], ]},
    {'node':'10', 'loc':[79.6240562919, 113.02734374999999], 'edges':[['9', 2], ['11', 1], ['stairs1', 1], ]},
    {'node':'11', 'loc':[38.272688535980976, 113.02734374999999], 'edges':[['10', 1], ['12', 2], ['stairs2', 1], ]},
    {'node':'12', 'loc':[38.272688535980976, -75.41015624999999], 'edges':[['11', 2],  ['9', 1], ['stairs2', 1], ]},
]



# Haystack settings
HAYSTACK_WHOOSH_PATH = PROJECT_DIR + "/../whoosh_index/"
HAYSTACK_SEARCH_RESULTS_PER_PAGE = 10
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'whoosh_index'),
        },
    }


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
