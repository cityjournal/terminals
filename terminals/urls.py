from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from main import views

urlpatterns = patterns('',
    # main app urls
    url(r'^$', views.index, name="home"),
    url(r'^home/$', views.home, name="index"),
    url(r'^about/$', views.about, name="about"),
    url(r'^test-about/$', views.test_about),
    # url(r'^$', TemplateView.as_view(template_name="index.html"), name="index"),
    # url(r'^shops/$', views.shops, name='shops'),
    url(r'^shops/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.shop, name="shop"),
    url(r'^gallery/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.gallery, name="gallery"),
    url(r'^categories/market/$', views.gurman, name='gurman'),
    url(r'^categories/(?P<type>[a-zA-Z0-9_.-]+)/$', views.categories, name='categories'),
    url(r'^categories/(?P<type>[a-zA-Z0-9_.-]+)/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.category, name='category'),
    # url(r'^events/$', views.events, name='events'),
    # url(r'^events/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.event, name="event"),
    # just for map testing without ajax
    url(r'^test-map/$', views.test_map),
    url(r'^test-map/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.test_map),
    url(r'^map/$', views.map, name="map"),
    url(r'^map/(?P<slug>[a-zA-Z0-9_.-]+)/$', views.map, name="map"),
    url(r'^get-map-data/$', views.get_map_data, name="get_map_data"),
    url(r'^navigate/', views.navigate, name="navigate"),
    # includes
    url(r'^upload/', include('fileupload.urls')),
    url(r'^news/', include('weblog.urls', app_name='weblog')),
    url(r'^events/', include('apps.eventcalendar.urls', app_name='eventcalendar')),
    url(r'^dashboard/', include('dashboard.urls', app_name='dashboard')),
    url(r'^adapp/', include('adapp.urls', app_name='adapp')),
    # party apps
    (r'^tinymce/', include('tinymce.urls')),
    (r'^mce_filebrowser/', include('mce_filebrowser.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # (r'^search/', include('haystack.urls')),
    url(r'^search/', views.CustomSearchView()),
    # django contrib apps
    (r'^accounts/login/$', 'django.contrib.auth.views.login'),
    (r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^lang/(?P<lang_code>)', 'main.views.langredirect', name="lang_switcher"),
)
# Allow rosetta to be used to add translations
if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        (r'^rosetta/', include('rosetta.urls')),
    )
if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns